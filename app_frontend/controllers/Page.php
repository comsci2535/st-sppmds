<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page {

	public $header = 'template/header';
	public $title = 'App name';
	public $head  = 'template/head';
	public $slider = 'template/slider';
	public $clear = 'template/clear';
	public $footer = 'template/footer';
	public $page = 'home';
}
