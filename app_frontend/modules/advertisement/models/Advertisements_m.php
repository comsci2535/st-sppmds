<?php defined('BASEPATH') OR exit('No direct script access allowed.');

class Advertisements_m extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  public function get_list($param)
  {

   $this->_condition($param);

   $query = $this->db
   ->select('a.*')
   ->from('advertisements a')
   ->get();

   return $query;
 }

 private function _condition($param) 

 {   

  if ( isset($param['active']) ) {
    $this->db->where('a.active', $param['active']);
  }           
  if ( isset($param['recycle']) )
    $this->db->where('a.recycle', $param['recycle']);

}

}