<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advertisement extends MX_Controller {

    private $_grpContent = 'advertisement';

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('advertisement/advertisements_m');
    }

    
    public function grid($page=null)
    {
        $input['type'] = $page;
        $input['active'] = '1';
        $input['recycle'] = '0';
        $info= [];
        $info = $this->advertisements_m->get_list($input)->result();

        $data['info'] = $info;
        $this->load->view('advertisement/grid', $data); 

    }

}