<footer id="footer" class="dark">

  <div id="copyrights">

    <div class="container clearfix">

      <div class="col_half">

        Copyrights &copy; 2019 All Rights Reserved by St-dev.<br>

        <div class="copyright-links"><a href="#">ข้อตกลงในการใช้งาน</a> / <a href="#">นโยบายความเป็นส่วนตัว</a></div>

      </div>

      <div class="col_half col_last tright">

        <div class="fright clearfix">

          <a href="https://facebook.com/SP-Premium-And-Design-2689875164363637" class="social-icon si-small si-borderless si-facebook">

            <i class="icon-facebook"></i>

            <i class="icon-facebook"></i>

          </a>

          <a href="#" class="social-icon si-small si-borderless si-gplus">

            <i class="icon-gplus"></i>

            <i class="icon-gplus"></i>

          </a>

        </div>

        <div class="clear"></div>

        <i class="icon-envelope2"></i> sp.pmds@gmail.com <span class="middot">&middot;</span> <i class="icon-headphones"></i>  02 003 5140 และ 0824727111

      </div>

    </div>

  </div>

</footer>