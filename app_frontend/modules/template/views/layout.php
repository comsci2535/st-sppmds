
<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<?php echo $this->load->view('template/header'); ?>

<body class="stretched">
	<div id="wrapper" class="clearfix">
		
		<?php echo $this->load->view('template/head'); ?>

		<?php
		if($pages->slider != null)
			echo Modules::run('slider/hero');

		if($pages->clear != null)
			echo $this->load->view('template/clear'); 
		?>

		<?php if($pages->page == 'home'){ ?>		
			<section id="content">
				<div class="content-wrap">
					<?php 


					echo Modules::run('advertisement/grid');

					echo $this->load->view($pages->content); 

				//product
					echo Modules::run('product/productlist');

				//
					echo Modules::run('portfolio/gallery');

					?>
				</div>
			</section>
			<?php 
		}else{
			
			if($pages->page == 'portfolio'){
				echo Modules::run('portfolio/gallery_full');
			}else{

				$this->load->view($pages->content,$data);
			}
		}
		?>
		<?php echo $this->load->view('template/footer'); ?>

	</div>

	<div id="gotoTop" class="icon-angle-up"></div>

	<?php echo $this->load->view('template/script'); ?>

</body>
</html>
