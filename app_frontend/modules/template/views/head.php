<header id="header">
	<div id="header-wrap">
		<div class="container clearfix">
			<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
			<div id="logo">
				<a href="<?=base_url()?>" class="standard-logo" data-dark-logo="<?=$this->config->item('convas'); ?>images/logo-dark.jpg"><img style="width: 50%;height: auto;margin-top: 10px;" src="<?=$this->config->item('convas'); ?>images/logo.jpg" alt="Canvas Logo"></a>

				<a href="<?=base_url()?>" class="retina-logo" data-dark-logo="<?=$this->config->item('convas'); ?>images/logo-dark@2x.jpg"><img style="width: 50%;height: auto;margin: auto;margin-top: 30px;" src="<?=$this->config->item('convas'); ?>images/logo@2x.jpg" alt="Canvas Logo"></a>
			</div>
			<nav id="primary-menu">
				<ul>
					<li class="<?=$pages->page == 'home' ? 'current' : '' ;?>"><a href="<?=base_url()?>"><div>หน้าแรก</div></a></li>
					<li class="<?=$pages->page == 'about' ? 'current' : '' ;?>"><a href="<?=base_url('about')?>"><div>เกี่ยวกับเรา</div></a></li>
					<li class="<?=$pages->page == 'product-detail' || $pages->page == 'category' ? 'current' : '' ;?>"><a  href=""><div>สินค้า</div></a>
						<ul>
							<?php foreach ($category as $item) {?>
							<li><a href="<?=base_url('product/category/').$item->slug?>"><div><?=$item->title?></div></a></li>
	                        <?php }?>
						</ul>
					</li>
					<li class="<?=$pages->page == 'portfolio' ? 'current' : '' ;?>"><a href="<?=base_urL('portfolio/full')?>"><div>ผลงานที่ผ่านมา</div></a></li>
					<li class="<?=$pages->page == 'contact' ? 'current' : '' ;?>"><a href="<?=base_urL('contact')?>"><div>ติดต่อเรา</div></a></li>
				</ul>
				<div id="top-search">
					<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
					<form action="<?=base_url('product/search/')?>" method="get">
						<input type="text" name="q" class="form-control" value="" placeholder="คำที่ค้นหา ..">
					</form>
				</div>
			</nav>
		</div>
	</div>
</header>