<section id="slider" class="slider-parallax" style="background-color: #fff;">
	<div id="oc-slider" class="owl-carousel carousel-widget" data-margin="0" data-items="1" data-pagi="false" data-loop="true" data-effect="fade" data-speed="450" data-autoplay="5000">
		<a href="#"><img src="<?=$this->config->item('convas'); ?>images/slider/full/1.jpg" alt=""></a>
		<a href="#"><img src="<?=$this->config->item('convas'); ?>images/slider/full/2.jpg" alt=""></a>
		<a href="#"><img src="<?=$this->config->item('convas'); ?>images/slider/full/3.jpg" alt=""></a>
	</div>
</section>