
<div class="section">
	<div class="container clearfix">
		<div class="row">
			<div class="col-md-9">
				<div class="heading-block bottommargin-sm">
					<h3>ยินดีต้อนรับเข้าสู้ SPPMDS</h3>
				</div>
				<p class="nobottommargin">Lasting change, stakeholders development Angelina Jolie world problem solving progressive. Courageous; social entrepreneurship change; accelerate resolve pursue these aspirations asylum.</p>
			</div>
			<div class="col-md-3">
				<a href="#" class="button button-3d button-dark button-large btn-block center" style="margin-top: 30px;">ขอใบเสนอราคา</a>
			</div>
		</div>
	</div>
</div>

<div class="container topmargin-sm clearfix">

	<div id="shop" class="shop grid-container clearfix" data-layout="fitRows">

		<div class="product clearfix">
			<div class="product-image">
				<a href="detailshop.php"><img src="<?=$this->config->item('convas'); ?>images/shop/dress/1.jpg" alt="Checked Short Dress"></a>
				<div class="sale-flash">50% Off*</div>
				<div class="product-overlay">
					<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
					<a href="<?=$this->config->item('convas'); ?>images/shop/dress/1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
				</div>
			</div>
			<div class="product-desc">
				<div class="product-title"><h3><a href="detailshop.php">Checked Short Dress</a></h3></div>
				<div class="product-price"><del>฿24.99</del> <ins>฿12.49</ins></div>
			</div>
		</div>

		<div class="product clearfix">
			<div class="product-image">
				<a href="detailshop.php"><img src="<?=$this->config->item('convas'); ?>images/shop/pants/1-1.jpg" alt="Slim Fit Chinos"></a>
				<div class="product-overlay">
					<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
					<a href="<?=$this->config->item('convas'); ?>images/shop/pants/1-1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
				</div>
			</div>
			<div class="product-desc">
				<div class="product-title"><h3><a href="detailshop.php">Slim Fit Chinos</a></h3></div>
				<div class="product-price">฿39.99</div>
			</div>
		</div>

		<div class="product clearfix">
			<div class="product-image">
				<a href="detailshop.php"><img src="<?=$this->config->item('convas'); ?>images/shop/shoes/1.jpg" alt="Dark Brown Boots"></a>
				<div class="product-overlay">
					<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
					<a href="<?=$this->config->item('convas'); ?>images/shop/shoes/1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
				</div>
			</div>
			<div class="product-desc">
				<div class="product-title"><h3><a href="detailshop.php">Dark Brown Boots</a></h3></div>
				<div class="product-price">฿49</div>
			</div>
		</div>

		<div class="product clearfix">
			<div class="product-image">
				<a href="detailshop.php"><img src="<?=$this->config->item('convas'); ?>images/shop/dress/2.jpg" alt="Light Blue Denim Dress"></a>
				<div class="product-overlay">
					<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
					<a href="<?=$this->config->item('convas'); ?>images/shop/dress/2.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
				</div>
			</div>
			<div class="product-desc">
				<div class="product-title"><h3><a href="detailshop.php">Light Blue Denim Dress</a></h3></div>
				<div class="product-price">฿19.95</div>
			</div>
		</div>

		<div class="product clearfix">
			<div class="product-image">
				<a href="detailshop.php"><img src="<?=$this->config->item('convas'); ?>images/shop/sunglasses/1.jpg" alt="Unisex Sunglasses"></a>
				<div class="sale-flash">Sale!</div>
				<div class="product-overlay">
					<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
					<a href="images/shop/sunglasses/1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
				</div>
			</div>
			<div class="product-desc">
				<div class="product-title"><h3><a href="detailshop.php">Unisex Sunglasses</a></h3></div>
				<div class="product-price"><del>฿19.99</del> <ins>฿11.99</ins></div>
			</div>
		</div>

		<div class="product clearfix">
			<div class="product-image">
				<a href="detailshop.php"><img src="<?=$this->config->item('convas'); ?>images/shop/tshirts/1.jpg" alt="Blue Round-Neck Tshirt"></a>
				<div class="product-overlay">
					<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
					<a href="images/shop/tshirts/1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
				</div>
			</div>
			<div class="product-desc">
				<div class="product-title"><h3><a href="detailshop.php">Blue Round-Neck Tshirt</a></h3></div>
				<div class="product-price">฿9.99</div>
			</div>
		</div>

		<div class="product clearfix">
			<div class="product-image">
				<a href="detailshop.php"><img src="<?=$this->config->item('convas'); ?>images/shop/watches/1.jpg" alt="Silver Chrome Watch"></a>
				<div class="product-overlay">
					<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
					<a href="<?=$this->config->item('convas'); ?>images/shop/watches/1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
				</div>
			</div>
			<div class="product-desc">
				<div class="product-title"><h3><a href="detailshop.php">Silver Chrome Watch</a></h3></div>
				<div class="product-price">฿129.99</div>
			</div>
		</div>

		<div class="product clearfix">
			<div class="product-image">
				<a href="detailshop.php"><img src="<?=$this->config->item('convas'); ?>images/shop/shoes/2.jpg" alt="Men Grey Casual Shoes"></a>
				<div class="sale-flash">Sale!</div>
				<div class="product-overlay">
					<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
					<a href="images/shop/shoes/2.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
				</div>
			</div>
			<div class="product-desc">
				<div class="product-title"><h3><a href="detailshop.php">Men Grey Casual Shoes</a></h3></div>
				<div class="product-price"><del>฿45.99</del> <ins>฿39.49</ins></div>
			</div>
		</div>

		<div class="product clearfix">
			<div class="product-image">
				<a href="detailshop.php"><img src="<?=$this->config->item('convas'); ?>images/shop/dress/3.jpg" alt="Pink Printed Dress"></a>
				<div class="sale-flash">Sale!</div>
				<div class="product-overlay">
					<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
					<a href="<?=$this->config->item('convas'); ?>images/shop/dress/3.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
				</div>
			</div>
			<div class="product-desc">
				<div class="product-title"><h3><a href="detailshop.php">Pink Printed Dress</a></h3></div>
				<div class="product-price">฿39.49</div>
			</div>
		</div>

		<div class="product clearfix">
			<div class="product-image">
				<a href="detailshop.php"><img src="<?=$this->config->item('convas'); ?>images/shop/pants/5.jpg" alt="Green Trousers"></a>
				<div class="sale-flash">Sale!</div>
				<div class="product-overlay">
					<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
					<a href="<?=$this->config->item('convas'); ?>images/shop/pants/5.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
				</div>
			</div>
			<div class="product-desc">
				<div class="product-title"><h3><a href="detailshop.php">Green Trousers</a></h3></div>
				<div class="product-price"><del>฿24.99</del> <ins>฿21.99</ins></div>
			</div>
		</div>

		<div class="product clearfix">
			<div class="product-image">
				<a href="detailshop.php"><img src="<?=$this->config->item('convas'); ?>images/shop/sunglasses/2.jpg" alt="Men Aviator Sunglasses"></a>
				<div class="product-overlay">
					<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
					<a href="<?=$this->config->item('convas'); ?>images/shop/sunglasses/2.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
				</div>
			</div>
			<div class="product-desc">
				<div class="product-title"><h3><a href="detailshop.php">Men Aviator Sunglasses</a></h3></div>
				<div class="product-price">฿13.49</div>
			</div>
		</div>

		<div class="product clearfix">
			<div class="product-image">
				<a href="detailshop.php"><img src="<?=$this->config->item('convas'); ?>images/shop/tshirts/4.jpg" alt="Black Polo Tshirt"></a>
				<div class="product-overlay">
					<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
					<a href="<?=$this->config->item('convas'); ?>images/shop/tshirts/4.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
				</div>
			</div>
			<div class="product-desc">
				<div class="product-title"><h3><a href="detailshop.php">Black Polo Tshirt</a></h3></div>
				<div class="product-price">฿11.49</div>
			</div>
		</div>

	</div><!-- #shop end -->

	<div class="line"></div>

</div>

<div class="container clearfix">
	<div class="heading-block center">
		<h3>ผลงานที่ผ่านมา</h3>
		<span></span>
	</div>
</div>

<div id="portfolio" class="portfolio portfolio-5 portfolio-nomargin portfolio-full portfolio-notitle clearfix" style="margin-bottom: -80px !important;">

	<article class="portfolio-item pf-media pf-icons">
		<div class="portfolio-image">
			<a href="#">
				<img src="<?=$this->config->item('convas'); ?>images/portfolio/4/1.jpg" alt="Open Imagination">
			</a>
		</div>
		<div class="portfolio-desc">
			<h3><a href="#">ชื่อสินค้า</a></h3>
			<span><a href="#">หมวดสินค้า</a></span>
		</div>
	</article>

	<article class="portfolio-item pf-illustrations">
		<div class="portfolio-image">
			<a href="#">
				<img src="<?=$this->config->item('convas'); ?>images/portfolio/4/2.jpg" alt="Locked Steel Gate">
			</a>
		</div>
		<div class="portfolio-desc">
			<h3><a href="#">ชื่อสินค้า</a></h3>
			<span><a href="#">หมวดสินค้า</a></span>
		</div>
	</article>

	<article class="portfolio-item pf-graphics pf-uielements">
		<div class="portfolio-image">
			<a href="#">
				<img src="<?=$this->config->item('convas'); ?>images/portfolio/4/3.jpg" alt="Mac Sunglasses">
			</a>
		</div>
		<div class="portfolio-desc">
			<h3><a href="#">ชื่อสินค้า</a></h3>
			<span><a href="#">หมวดสินค้า</a></span>
		</div>
	</article>

	<article class="portfolio-item pf-icons pf-illustrations">
		<div class="portfolio-image">
			<a href="#"><img src="<?=$this->config->item('convas'); ?>images/portfolio/4/4-1.jpg" alt="Morning Dew"></a>
		</div>
		<div class="portfolio-desc">
			<h3><a href="#">ชื่อสินค้า</a></h3>
			<span><a href="#">หมวดสินค้า</a></span>
		</div>
	</article>

	<article class="portfolio-item pf-uielements pf-media">
		<div class="portfolio-image">
			<a href="#">
				<img src="<?=$this->config->item('convas'); ?>images/portfolio/4/5.jpg" alt="Console Activity">
			</a>
		</div>
		<div class="portfolio-desc">
			<h3><a href="#">ชื่อสินค้า</a></h3>
			<span><a href="#">หมวดสินค้า</a></span>
		</div>
	</article>

</div>


