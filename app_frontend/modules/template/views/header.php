<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="SPPMDS" />
<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?=$this->config->item('convas'); ?>css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="<?=$this->config->item('convas'); ?>style.css" type="text/css" />
<link rel="stylesheet" href="<?=$this->config->item('convas'); ?>css/dark.css" type="text/css" />
<link rel="stylesheet" href="<?=$this->config->item('convas'); ?>css/font-icons.css" type="text/css" />
<link rel="stylesheet" href="<?=$this->config->item('convas'); ?>css/animate.css" type="text/css" />
<link rel="stylesheet" href="<?=$this->config->item('convas'); ?>css/magnific-popup.css" type="text/css" />
<link rel="stylesheet" href="<?=$this->config->item('convas'); ?>css/responsive.css" type="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!--[if lt IE 9]>
  <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
	<title><?=$pages->title?></title>
</head>