<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Home extends MX_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	public function __construct() {

		parent::__construct();

		$this->load->model('product/product_m');

	}



	public function index()

	{

		Modules::run('track/front','');

		$pages = new Page();

		$pages->title = 'Home - Shop | SPPMDS';

		$pages->content = 'home/content';

		$pages->page = 'home';



		$data['pages'] = $pages;



		echo Modules::run('template/index',$data);

	}



	public function page_not_found()

	{

		$category = $this->product_m->get_category()->result();

        $data['category'] = $category;

		$this->load->view('home/page-not-found',$data); 

	}	

}

class Page {

	public $header = 'template/header';
	public $title = 'App name';
	public $head  = 'template/head';
	public $slider = 'template/slider';
	public $clear = 'template/clear';
	public $footer = 'template/footer';
	public $page = 'home';
}


