<section id="page-title">

	<div class="container clearfix">

		<h1>ผลงานที่ผ่านมา</h1>

		<span>ผลงานของเราที่ผ่านมา</span>

		<ol class="breadcrumb">

			<li><a href="<?=base_url()?>">หน้าแรก</a></li>

			<li class="active">ผลงานที่ผ่านมา</li>

		</ol>

	</div>

</section>

<div class="clear"></div>

<section id="content">



	<div class="content-wrap">



		<?php  if(sizeof($info) != 0) {?>

			<div class="container clearfix">

				<div class="heading-block center">

					<h3>ผลงานที่ผ่านมา</h3>

					<span></span>

				</div>

			</div>





			<div id="portfolio" class="portfolio portfolio-5 portfolio-nomargin portfolio-full portfolio-notitle clearfix" style="margin-bottom: -80px !important;">



				<?php foreach ($info as $item) { ?>



					<article class="portfolio-item pf-media pf-icons">

						<div class="portfolio-image">

							<a href="<?=base_url().$item->portfolio_img?>" class="item-quick-view" data-lightbox="image">

								<img src="<?=base_url().$item->portfolio_img?>" alt="Open Imagination">

							</a>

						</div>

						<div class="portfolio-desc">

							<h3><a href="<?=base_url().$item->portfolio_img?>" class="item-quick-view" data-lightbox="image"><?=$item->title?></a></h3>

							<span><a href="<?=base_url().$item->portfolio_img?>" class="item-quick-view" data-lightbox="image"><?=$item->portfolio_dis?></a></span>

						</div>

					</article>

				<?php } ?>



			</div>

		<?php } ?>





	</div><!-- #shop end -->





</div>







<!--  -->

