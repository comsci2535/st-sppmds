<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Portfolio extends MX_Controller {



    private $_grpContent = 'banner';



    public function __construct() 

    {

        parent::__construct();

        $this->load->model('portfolio/portfolio_m');

    }



    

    public function gallery($page=null)

    {

        $input['type'] = $page;

        $input['active'] = '1';

        $input['recycle'] = '0';

        $info= [];

        $info = $this->portfolio_m->get_list($input)->result();



        $data['info'] = $info;

        $this->load->view('portfolio/grid', $data); 



    }



    public function gallery_full($page=null)

    {

        Modules::run('track/front', '');

        $input['type'] = $page;

        $input['active'] = '1';

        $input['recycle'] = '0';

        $info= [];

        $info = $this->portfolio_m->get_list($input)->result();



        $data['info'] = $info;

        $this->load->view('portfolio/full-page', $data); 



    }



    public function full($page=null)

    {



        $pages = new Portfolio_page();

        $pages->title = 'ค้นหา | SPPMDS';

        $pages->content = 'portfolio/grid';

        $pages->page = 'portfolio';



        $data['pages'] = $pages;



        echo Modules::run('template/index',$data);



    }



}

class Portfolio_page {

    public $header = 'template/header';
    public $title = 'App name';
    public $head  = 'template/head';
    public $slider = 'template/slider';
    public $clear = 'template/clear';
    public $footer = 'template/footer';
    public $page = 'home';
}