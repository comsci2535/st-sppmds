<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<?php echo $this->load->view('template/header'); ?>

<body class="stretched">
	<div id="wrapper" class="clearfix">
		<?php echo $this->load->view('template/head'); ?>

		<section id="page-title">
			<div class="container clearfix">
				<h1><?php echo $send; ?></h1>
				<span><a href="<?=base_url()?>">กลับสู่หน้าแรก</a></span>
				
			</div>
		</section>
		<div class="clear"></div>

	<?php echo $this->load->view('template/footer'); ?>
	
</div>

<div id="gotoTop" class="icon-angle-up"></div>

<?php echo $this->load->view('template/script'); ?>

</body>
</html>
