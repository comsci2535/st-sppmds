

<!DOCTYPE html>

<html dir="ltr" lang="en-US">



<?php echo $this->load->view('template/header'); ?>



<body class="stretched">

	<div id="wrapper" class="clearfix">

		<?php echo $this->load->view('template/head'); ?>



		<section id="page-title">

			<div class="container clearfix">

				<h1>ขอใบเสนอราคา</h1>

				<span>ขอใบเสนอราคา</span>

				<ol class="breadcrumb">

					<li><a href="<?=base_url()?>">หน้าแรก</a></li>

					<li class="active">ขอใบเสนอราคา</li>

				</ol>

			</div>

		</section>

		<div class="clear"></div>



		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">



					<!--Section: Contact v.2-->

					<section class="mb-4">



						<div class="row">



							<!--Grid column-->

							<div class="col-md-12 mb-md-0 mb-5">

								

								<div class="alert alert-danger" id="alert">

									<strong><div id="status"></div></strong>

								</div>

								<form id="contact-form" name="contact-form" action="<?=base_url('contact/mail')?>" method="POST">

									<div class="row">
										<div class="col-md-12">

											<div class="md-form mb-0">

												<input type="text" id="store" name="store" class="form-control" required>

												<label for="store" class="">ชื่อบริษัท/ห้างร้าน <span style="color: red">*</span></label>

											</div>
										</div>
									</div> 	


									<!--Grid row-->

									<div class="row">



										<!--Grid column-->

										<div class="col-md-6">

											<div class="md-form mb-0">

												<input type="text" id="name" name="name" class="form-control" required>

												<label for="name" class="">ชื่อผู้ติดต่อ <span style="color: red">*</span></label>

											</div>

										</div>

										
										<!--Grid column-->



										<!--Grid column-->

										<div class="col-md-6">

											<div class="md-form mb-0">

												<input type="text" id="email" name="email" class="form-control" required >

												<label for="email" class="">email ติดต่อกลับ <span style="color: red">*</span></label>

											</div>

										</div>

										<!--Grid column-->



									</div>

									<!--Grid row-->
									<div class="row">
										<div class="col-md-6">

											<div class="md-form mb-0">

												<input type="text"  id="line" name="line" class="form-control" >

												<label for="line" class="">Line</label>

											</div>

										</div>

										<div class="col-md-6">

											<div class="md-form mb-0">

												<input type="text"  id="tell" name="tell" class="form-control" required>

												<label for="tell" class="">เบอร์โทร <span style="color: red">*</span></label>

											</div>

										</div>
									</div>

									<div class="row">			
										<div class="col-md-12">

											<div class="md-form mb-0">

												<textarea type="text" rows="5" id="address" name="address" rows="2" class="form-control md-textarea" required></textarea>

												<label for="address" class="">ที่อยู่ <span style="color: red">*</span></label>

											</div>

										</div>
									</div>


									<!--Grid row-->

									<div class="row">

										<div class="col-md-6">

											<div class="md-form mb-0">

												<input type="text" value="สนใจสินค้า รหัส : <?=$product_code?>" id="subject" name="subject" class="form-control" >

												<label for="subject" class="">รหัสสินค้าที่สอบถาม</label>

											</div>

										</div>

										<div class="col-md-6">

											<div class="md-form mb-0">

												<input type="number" min="1"  id="qty" name="qty" class="form-control" >

												<label for="qty" class="">จำนวน</label>

											</div>

										</div>

									</div>

									<!--Grid row-->
									<div class="row">

										
									</div>



									<!--Grid row-->

									<div class="row">



										<!--Grid column-->

										<div class="col-md-12">



											<div class="md-form">

												<textarea type="text" rows="5" id="message" name="message" rows="2" class="form-control md-textarea" ></textarea>

												<label for="message">รายละเอียด</label>

											</div>



										</div>

									</div>

									<!--Grid row-->



								</form>



								<div class="text-center text-md-left">

									<a class="btn btn-primary" onclick="validateForm()">Send</a>

								</div>

								

							</div>

							<!--Grid column-->



							<!--Grid column-->

							<!--Grid column-->



						</div>



					</section>

					<!--Section: Contact v.2-->



					



				</div><!-- Contact Info End -->



			</div>



		</div>



	</section>



	<?php echo $this->load->view('template/footer'); ?>

	

</div>



<div id="gotoTop" class="icon-angle-up"></div>



<?php echo $this->load->view('template/script'); ?>

<script>

	

	$( document ).ready(function() {

		var alert = $('#alert');

		alert.hide()



	});



	function validateForm() {

		var alert = $('#alert');

		var name =  document.getElementById('name').value;

		if (name == "") {

			alert.show()

			document.getElementById('status').innerHTML = "กรุณาระบุชื่อ-นามสกุล";

			return false;

		}

		var email =  document.getElementById('email').value;

		if (email == "") {

			alert.show()

			document.getElementById('status').innerHTML = " กรุณาระบุ Email";

			return false;

		} else {

			alert.show()

			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

			if(!re.test(email)){

				document.getElementById('status').innerHTML = "Email ไม่ถูกต้อง";

				return false;

			}

		}

		var subject =  document.getElementById('subject').value;

		if (subject == "") {

			alert.show()

			document.getElementById('status').innerHTML = "กรุณาระบุ Subject";

			return false;

		}

		var message =  document.getElementById('message').value;

		if (message == "") {

			alert.show()

			document.getElementById('status').innerHTML = "กรุณาระบุ Message ";

			return false;

		}

		document.getElementById('status').innerHTML = "Sending...";

		document.getElementById('contact-form').submit();



	}

</script>



</body>

</html>

