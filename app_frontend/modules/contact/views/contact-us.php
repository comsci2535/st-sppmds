
<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<?php echo $this->load->view('template/header'); ?>

<body class="stretched">
	<div id="wrapper" class="clearfix">
		<?php echo $this->load->view('template/head'); ?>

		<section id="page-title">
			<div class="container clearfix">
				<h1>ติดต่อเรา</h1>
				<span>ติดต่อเรา</span>
				<ol class="breadcrumb">
					<li><a href="<?=base_url()?>">หน้าแรก</a></li>
					<li class="active">ติดต่อเรา</li>
				</ol>
			</div>
		</section>
        <div class="clear"></div>

        <section id="content">
			<div class="content-wrap">
				<div class="container clearfix">

					<div class="col_half">
						<div id="fb-root"></div>
						<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v3.2&appId=289137874657&autoLogAppEvents=1"></script>
						<div class="fb-page" data-href="https://web.facebook.com/SP-Premium-And-Design-2689875164363637/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://web.facebook.com/SP-Premium-And-Design-2689875164363637/" class="fb-xfbml-parse-ignore"><a href="https://web.facebook.com/SP-Premium-And-Design-2689875164363637/">St-dev</a></blockquote></div>
					</div>

					<div class="col_half col_last">
						<section id="google-map" class="gmap" style="height: 450px;">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3876.587164598116!2d100.4100850148297!3d13.682848990392122!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e2bd6605234665%3A0xf6fb264b7e1b68a0!2z4LiV4Lil4Liy4LiU4LiZ4LmJ4Liz4Liq4Liz4LmA4Lie4LmH4LiHMg!5e0!3m2!1sth!2sth!4v1554319788145!5m2!1sth!2sth" frameborder="0" style="border:0" allowfullscreen></iframe>
						</section>
					</div>

					<div class="clear"></div>

					<div class="row clear-bottommargin">

						<div class="col-md-3 col-sm-6 bottommargin clearfix">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href="https://goo.gl/maps/P5D8NsVjQBG2"><i class="icon-map-marker2"></i></a>
								</div>
								<h3>ถนน กัลปพฤกษ์<span class="subtitle">แขวง บางบอน, เขต บางบอน</span></h3>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 bottommargin clearfix">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href="tel:"><i class="icon-phone3"></i></a>
								</div>
								<h3>โทรติดต่อ<span class="subtitle">089 123 4567</span></h3>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 bottommargin clearfix">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href="mailto:sppmds@gmail.com"><i class="icon-email3"></i></a>
								</div>
								<h3>ส่งอีเมลหาเรา<span class="subtitle">sppmds@gmail.com</span></h3>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 bottommargin clearfix">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-facebook"></i></a>
								</div>
								<h3>ติดต่อ Facebook<span class="subtitle">SP Premium And Design</span></h3>
							</div>
						</div>

					</div><!-- Contact Info End -->

				</div>

			</div>

		</section>

		<?php echo $this->load->view('template/footer'); ?>
		
	</div>

	<div id="gotoTop" class="icon-angle-up"></div>

	<?php echo $this->load->view('template/script'); ?>

</body>
</html>
