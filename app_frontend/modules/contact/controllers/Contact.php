<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Contact extends MX_Controller {



    private $_grpContent = 'contact';



    public function __construct() 

    {

        parent::__construct();

        $this->load->model('product/product_m');

    }



    

    public function index($page=null)

    {
        Modules::run('track/front', '');

        $category = $this->product_m->get_category()->result();

        $data['category'] = $category;

       

        $pages = new Page();

        $pages->title = 'ติดต่อเรา | SPPMDS';

        $pages->page = 'contact';

        $data['pages'] = $pages;

        $this->load->view('contact/contact-us',$data);

    }



    public function request($product_code=null)

    {



        $category = $this->product_m->get_category()->result();

        $data['category'] = $category;

     

        $pages = new Page();

        $pages->title = 'ขอใบเสนอราคา | SPPMDS';

        $pages->page = 'contact';

        $data['pages'] = $pages;

        $data['product_code']  = $product_code;



        $this->load->view('contact/request',$data);

    }



    public function mail()

    {

        $category = $this->product_m->get_category()->result();

        $data['category'] = $category;

     

        if(isset( $_POST['name']))

            $name = $_POST['name'];

        if(isset( $_POST['email']))

            $email = $_POST['email'];

        if(isset( $_POST['message']))

            $message = $_POST['message'];

        if(isset( $_POST['subject']))

            $subject = $_POST['subject'];

        if ($name === ''){

            echo "Name cannot be empty.";

            die();

        }

        if ($email === ''){

            echo "Email cannot be empty.";

            die();

        } else {

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)){

                echo "Email format invalid.";

                die();

            }

        }

        if ($subject === ''){

            echo "Subject cannot be empty.";

            die();

        }

        if ($message === ''){

            echo "Message cannot be empty.";

            die();

        }

        $content="From: $name \nEmail: $email \nMessage: $message";

        $recipient = "sppmds@gmail.com";

        //$recipient = "zun7441.yru@gmail.com";

        $mailheader = "From: $email \r\n";

        mail($recipient, $subject, $content, $mailheader) or die("Error!");

        $data['send'] = "ส่งข้อมูลเรียบร้อย !";

        $this->load->view('contact/send-mail',$data);



    }



}


class Page {

    public $header = 'template/header';
    public $title = 'App name';
    public $head  = 'template/head';
    public $slider = 'template/slider';
    public $clear = 'template/clear';
    public $footer = 'template/footer';
    public $page = 'home';
}