

<!DOCTYPE html>

<html dir="ltr" lang="en-US">



<?php echo $this->load->view('template/header'); ?>



<body class="stretched">

	<div id="wrapper" class="clearfix">

		<?php echo $this->load->view('template/head'); ?>



		<section id="page-title">

			<div class="container clearfix">

				<h1>เกี่ยวกับเรา</h1>

				<span>เกี่ยวกับเรา</span>

				<ol class="breadcrumb">

					<li><a href="<?=base_url()?>">หน้าแรก</a></li>

					<li class="active">เกี่ยวกับเรา</li>

				</ol>

			</div>

		</section>

		<div class="clear"></div>



		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					
					<?php echo html_entity_decode($info); ?>

				</div><!-- Contact Info End -->



			</div>



		</div>



	</section>



	<?php echo $this->load->view('template/footer'); ?>

	

</div>



<div id="gotoTop" class="icon-angle-up"></div>



<?php echo $this->load->view('template/script'); ?>



</body>

</html>

