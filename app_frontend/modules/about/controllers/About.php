<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class About extends MX_Controller {



    private $_grpContent = 'about';



    public function __construct() 

    {

        parent::__construct();

        //$this->load->model('about/about_m');

        $this->load->model('product/product_m');

    }



    

    public function index($page=null)

    {
        Modules::run('track/front','');

        $category = $this->product_m->get_category()->result();

        $query=$this->db
                ->select('a.*')
                ->from('config a')
                ->where('a.type', 'about')
                ->where('a.variable', 'aboutUs')
                ->get()->result_array();


        

        $data['category'] = $category;

        //include(APPPATH.'controllers\page.php');

        $pages = new Page();

        $pages->title = 'เกี่ยวกับเรา | SPPMDS';

        $pages->page = 'about';


        $data['pages'] = $pages;
        $data['info']=$query[0]['value'];

        $this->load->view('about/about-us',$data);

    }



}

class Page {

    public $header = 'template/header';
    public $title = 'App name';
    public $head  = 'template/head';
    public $slider = 'template/slider';
    public $clear = 'template/clear';
    public $footer = 'template/footer';
    public $page = 'home';
}