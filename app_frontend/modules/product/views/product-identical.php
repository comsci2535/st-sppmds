<div class="widget clearfix">

	<h4>สินค้าในหมวดเดียวกัน</h4>

	<div class="widget-last-view">

		<?php foreach ($products_identical as $identical) { ?>

		<div class="spost clearfix">

			<div class="entry-image">

				<a href="<?=base_url('product/detail/').$identical->slug?>"><img src="<?=base_url().$identical->img[0]->file?>" alt="Image"></a>

			</div>

			<div class="entry-c">

				<div class="entry-title">

					<h4><a href="<?=base_url('product/detail/').$identical->slug?>"><?=$identical->title?></a></h4>

				</div>

				<ul class="entry-meta">

					<li class="color"><?=$identical->price.' บาท'?></li>

				</ul>

			</div>

		</div>

	<?php } ?>



	</div>

</div>

