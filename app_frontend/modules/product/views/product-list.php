<div class="container topmargin-sm clearfix">

	<div id="shop" class="shop grid-container clearfix" data-layout="fitRows">
        
		<?php //print_r($products);
		 if(sizeof($products) != 0){
			foreach ($products as $product) {
				// var_dump($product);
		?>

		<div class="product clearfix">
			<div class="product-image">
				<a href="<?=base_url('product/detail/').$product->slug?>"><img src="<?=base_url().$product->img[0]->file?>" alt="Checked Short Dress"></a>
				<?php if($product->sell != 0):?>
				<div class="sale-flash"><?=$product->sell?> %</div>
			<?php endif; ?>
				<div class="product-overlay">
					<a href="<?=base_url('product/detail/').$product->slug?>" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
					<a href="<?=base_url().$product->img[0]->file?>" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
				</div>
			</div>
			<div class="product-desc">
				<div class="product-title"><h3><a href="<?=base_url('product/detail/').$product->slug?>"><?=$product->title?></a></h3></div>
				<div class="product-price">
					<?php if($product->sell != 0):?><del>฿<?=number_format($product->price,0) ?></del><?php endif; ?>
					 <ins>฿<?=($product->sell != 0) ? $product->price - (($product->price * $product->sell)/100)  : $product->price ; ?></ins></div>
			</div>
		</div>

		<?php } }else{ ?>

			<div class="product clearfix">
				<h1 id="text">ยังไม่มีสินค้า</h1>
			</div>
	   <?php }?>		


	</div><!-- #shop end -->

	<div class="line"></div>

</div>



<!--  -->
