<div class="widget clearfix">

	<h4>สินค้าแนะนำ</h4>

	<div id="post-list-footer">

		<?php foreach ($products_recommend as $recommend) { ?>

		<div class="spost clearfix">

			<div class="entry-image">

				<a href="<?=base_url('product/detail/').$recommend->slug?>"><img src="<?=base_url().$recommend->img[0]->file?>" alt="Image"></a>

			</div>

			<div class="entry-c">

				<div class="entry-title">

					<h4><a href="<?=base_url('product/detail/').$recommend->slug?>"><?=$recommend->title?></a></h4>

				</div>

				<ul class="entry-meta">

					<li class="color"><?=$recommend->price.' บาท'?></li>

				</ul>

			</div>

		</div>



	<?php } ?>



	</div>

</div>