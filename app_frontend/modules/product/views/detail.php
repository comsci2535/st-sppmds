<section id="page-title">

	<div class="container clearfix">

		<h1><?=$products[0]->title?></h1>

		<span>เลือกสินค้าตามใจคุณ</span>

		<ol class="breadcrumb">

			<li><a href="<?=base_url()?>">หน้าแรก</a></li>

			<li class="active">สินค้า</li>

		</ol>

	</div>

</section>



<div class="clear"></div>



<section id="content">

	<div class="content-wrap">



		<div class="container clearfix">

			<div class="postcontent nobottommargin clearfix col_last">

				<div class="single-product">

					<div class="product">

						<div class="col_half">

							<div class="product-image">

								<div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">

									<div class="flexslider">

										<div class="slider-wrap" data-lightbox="gallery">

											<?php foreach($products as $item) { 

												foreach ( $item->img as $img) {

													?>

													<div class="slide" data-thumb="<?=base_url().$img->file?>">

														<a href="<?=base_url().$img->file?>" title="Pink Printed Dress - Front View" data-lightbox="gallery-item">

															<img src="<?=base_url().$img->file?>" alt="Pink Printed Dress">

														</a>

													</div>

												<?php } }?>



											</div>

										</div>

									</div>

									<?php if($products[0]->sell != 0 ){;?>

										<div class="sale-flash"><?=$products[0]->sell?> %</div>

									<?php }?>

								</div>

							</div>



							<div class="col_half col_last product-desc">



								<div class="col_full nobottommargin">

									<div class="tabs clearfix nobottommargin" id="tab-1">

										<ul class="tab-nav clearfix">

											<?php $i=1; foreach ($products[0]->feature as $item) {?>

												<li><a href="#tabs-<?=$i?>"><i class="icon-align-justify2"></i><span class="hidden-xs"><?=$item->feature_name?></span></a></li>

												<?php $i++; } ?>

											</ul>

											<div class="tab-container">



												<?php $i=1; foreach ($products[0]->feature as $item) {?>

													<div class="tab-content clearfix" id="tabs-<?=$i?>">

														<table class="table table-striped table-bordered">

															<tbody>

																<tr>

																	<td><?=$item->feature_dis1?></td>

																	<td><?=$item->feature_price1.' บาท'?></td>

																</tr>

																<tr>

																	<td><?=$item->feature_dis2?></td>

																	<td><?=$item->feature_price2.' บาท'?></td>

																</tr>

																<tr>

																	<td><?=$item->feature_dis3?></td>

																	<td><?=$item->feature_price3.' บาท'?></td>

																</tr>

															</tbody>

														</table>

													</div>

													<?php $i++; }?>



												</div>

											</div>

										</div>



										<div class="clear"></div>

										<div class="line"></div>





										<a target="_blank" href="https://m.me/SakidloCode" class="button button-rounded button-reveal button-large button-green button-light tright"><i class="icon-line-arrow-right"></i><span style="color: #fff;">สอบถามรายละเอียดสินค้า</span></a>

										<a href="<?=base_url('contact/request/').$products[0]->code?>" class="button button-rounded button-reveal button-large button-red button-light tright"><i class="icon-line-arrow-right"></i><span style="color: #fff;">ขอใบเสนอราคา</span></a>



										<div class="clear"></div>

										<div class="line"></div>



										<p><?=$products[0]->excerpt?></p>



										<div class="panel panel-default product-meta">

											<div class="panel-body">

												<span itemprop="productID" class="sku_wrapper">SKU: <span class="sku"><?=$products[0]->sku?></span></span>

												<span class="posted_in">Category: <a href="#" rel="tag"></a><?=$products[0]->cat_title?></span>



											</div>

										</div>



										<div class="si-share noborder clearfix">

											<span>Share:</span>

											<div>

												<a href="javascript: openwindow()" class="social-icon si-borderless si-facebook" >

													<i class="icon-facebook"></i>

													<i class="icon-facebook"></i>

												</a>

											</div>

										</div>



									</div>



									<div class="col_full nobottommargin">



										<?=html_entity_decode($products[0]->detail)?>
										<br>

										<img src="<?=base_url().$products[0]->img[0]->file?>" alt="Pink Printed Dress">



										<div class="line"></div>



									</div>

								</div>

							</div>



							<div class="clear"></div>







						</div>



						<div class="sidebar nobottommargin">

							<div class="sidebar-widgets-wrap">



								<div class="widget widget_links clearfix">

									<h4>หมวดสินค้า</h4>

									<ul>

										<?php foreach($categorys as $value) { ?>

											<li><a href="<?=base_url().'product/category/'.$value->slug?>"><div><?=$value->title?></div></a></li>

										<?php }?>

									</ul>

								</div>



								<?php echo Modules::run('product/recommend'); ?>



								<?php echo Modules::run('product/identical',$products[0]->cat_slug); ?>

                                

                                <!-- facebook -->

								<div class="widget clearfix">

									<div id="fb-root"></div>

									<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v3.2&appId=289137874657&autoLogAppEvents=1"></script>

									<div class="fb-page" data-href="https://facebook.com/SP-Premium-And-Design-2689875164363637" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://facebook.com/SP-Premium-And-Design-2689875164363637" class="fb-xfbml-parse-ignore"><a href="https://facebook.com/SP-Premium-And-Design-2689875164363637">SP Premium And Design</a></blockquote></div>

								</div>

								<!-- #facebook -->



								<!-- <div class="widget clearfix">

									<div id="oc-clients-full" class="owl-carousel image-carousel carousel-widget" data-items="1" data-margin="10" data-loop="true" data-nav="false" data-autoplay="5000" data-pagi="false">

										<div class="oc-item"><a href="#"><img src="images/clients/1.png" alt="Clients"></a></div>

										<div class="oc-item"><a href="#"><img src="images/clients/2.png" alt="Clients"></a></div>

										<div class="oc-item"><a href="#"><img src="images/clients/3.png" alt="Clients"></a></div>

										<div class="oc-item"><a href="#"><img src="images/clients/4.png" alt="Clients"></a></div>

										<div class="oc-item"><a href="#"><img src="images/clients/5.png" alt="Clients"></a></div>

										<div class="oc-item"><a href="#"><img src="images/clients/6.png" alt="Clients"></a></div>

										<div class="oc-item"><a href="#"><img src="images/clients/7.png" alt="Clients"></a></div>

										<div class="oc-item"><a href="#"><img src="images/clients/8.png" alt="Clients"></a></div>

									</div>

								</div> -->

							</div>



						</div><!-- .sidebar end -->



					</div>

				</div>

			</section>

			<SCRIPT language="JavaScript1.2">

				function openwindow()

				{

					window.open("https://www.facebook.com/sharer/sharer.php?u=<?=base_url().'product/detail/'.$products[0]->slug?>","mywindow","menubar=1,resizable=1,width=450,height=250");

				}

			</SCRIPT>







