<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Product extends MX_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	public function __construct() {

		parent::__construct();

		$this->load->model('product/product_m');

	}



	public function index()

	{


		echo Modules::run('template/index',$pages);

	}



	public function detail($slug = '')

	{


		$pages = new Product_page();

		$pages->title = 'สินค้า | SPPMDS';

		$pages->slider = null;

		$pages->content = 'product/detail';

		$pages->page = 'product-detail';



		$products = $this->product_m->get_rows($slug)->result();
        
        Modules::run('track/front', $products[0]->product_id);
        
		if($products):

			foreach($products as $item) {

				$item->img = $this->product_m->get_product_img($item->product_id,5)->result();

				$item->feature = $this->product_m->get_product_feature($item->product_id,5)->result();

			}

		endif;



		$data['products'] = $products;

		$data['pages'] = $pages;

		$data['categorys'] = $this->product_m->get_category()->result();



		echo Modules::run('template/index',$data);

	}



	public function productlist($slug='')

	{



		$products = $this->product_m->get_rows()->result();

		if($products):

			foreach($products as $item) {

				$item->img = $this->product_m->get_product_img($item->product_id)->result();

			}

		endif;



		$data['products'] = $products;

		$this->load->view('product/product-list',$data); 

	}



	public function recommend()

	{

		$products = $this->product_m->get_rows('',3)->result();

		if($products):

			foreach($products as $item) {

				$item->img = $this->product_m->get_product_img($item->product_id,5)->result();

				$item->feature = $this->product_m->get_product_feature($item->product_id,5)->result();

			}

		endif;



		$data['products_recommend'] = $products;



		$this->load->view('product/product-recommend',$data); 

	}



	public function identical($category_id)

	{

		$products = $this->product_m->get_category_rows($category_id,3)->result();

		if($products):

			foreach($products as $item) {

				$item->img = $this->product_m->get_product_img($item->product_id,5)->result();

				$item->feature = $this->product_m->get_product_feature($item->product_id,5)->result();

			}

		endif;



		$data['products_identical'] = $products;



		$this->load->view('product/product-identical',$data); 

	}



	public function product_category_list($category='')

	{



		$products = $this->product_m->get_category_rows($category)->result();

		if($products):

			foreach($products as $item) {

				$item->img = $this->product_m->get_product_img($item->product_id)->result();

			}

		endif;



		$data['products'] = $products;

		$this->load->view('product/product-list',$data); 

	}



	public function category($slug='')

	{




		$pages = new Product_page();

		$pages->title = 'หมวดหมู่สินค้า| SPPMDS';

		$pages->content = 'product/category';

		$pages->page = 'category';

		$pages->sub_page = $slug;



		$data['pages'] = $pages;

		$data['slug'] = $slug;

		$data['products'] = $products;

		echo Modules::run('template/index',$data);

	}



	public function search()

	{ 

		$key = $_GET['q'];

		$products = $this->product_m->get_search($key)->result();



		if($products):

			foreach($products as $item) {

				$item->img = $this->product_m->get_product_img($item->product_id)->result();

			}

		endif;




		$pages = new Product_page();

		$pages->title = 'ค้นหา | SPPMDS';

		$pages->content = 'product/search';

		$pages->page = 'search';

		$pages->sub_page = $slug;



	    $data['products'] = $products;

	    $data['pages'] = $pages;



	    echo Modules::run('template/index',$data);

	}

}


class Product_page {

    public $header = 'template/header';
    public $title = 'App name';
    public $head  = 'template/head';
    public $slider = 'template/slider';
    public $clear = 'template/clear';
    public $footer = 'template/footer';
    public $page = 'home';
}





