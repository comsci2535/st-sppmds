<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($slug='',$limit='') 
    {
        if($slug != '')
            $this->db->where('p.slug', $slug);
        if($limit != '')
            $this->db->limit( $limit );

        $query = $this->db
        ->select('p.*,c.title as cat_title ,c.slug as cat_slug ,c.category_id')
        ->from('product p')
        ->where('p.active',1)
        ->where('p.recycle',0)
        ->join('category c','p.category_id = c.category_id','LEFT')
        ->order_by('p.updated_at','DESC')
        ->get();


        return $query;
    }
    
    public function get_category_rows($slug ='',$limit='') 
    {
        if($slug != '')
            $this->db->where('c.slug', $slug);
        if($limit != '')
            $this->db->limit( $limit );

        $query = $this->db
        ->select('p.*,c.title as cat_title ,c.slug as cat_slug ')
        ->from('product p')
        ->where('p.active',1)
        ->where('p.recycle',0)
        ->join('category c','p.category_id = c.category_id','LEFT')
        ->order_by('p.updated_at','DESC')
        ->get();
        
        return $query;
    }

    public function get_product_img($code,$limit = 1) 
    {
        $query = $this->db
        ->select('*')
        ->from('product_img')
        ->where('product_id',$code)
        ->limit($limit)
        ->get();

        return $query;
    }

    public function get_product_feature($code,$limit = 1) 
    {
        $query = $this->db
        ->select('*')
        ->from('product_feature')
        ->where('product_id',$code)
        ->limit($limit)
        ->get();

        return $query;
    }

    public function get_category($code='') 
    {
        if($code != '')
            $this->db->where('category_id', $code);

        $query = $this->db
        ->select('*')
        ->from('category')
        ->where('active',1)
        ->where('recycle',0)
        ->get();

        return $query;
    }

    public function get_detail_product($slug='')
    {
        if($slug != '')
            $this->db->where('p.slug', $slug);

        $query = $this->db
        ->select('p.*')
        ->from('product p')
        ->where('active',1)
        ->order_by('updated_at','DESC')
        ->get();

        return $query;
    }

    public function get_search($key ='') 
    {
        if($key != ''){
            $this->db->or_like('p.slug', $key);
            $this->db->or_like('c.slug', $key);
        }
        
        $query = $this->db
        ->select('p.*,c.title as cat_title ,c.slug as cat_slug ')
        ->from('product p')
        ->where('p.active',1)
        ->where('p.recycle',0)
        ->join('category c','p.category_id = c.category_id','LEFT')
        ->order_by('p.updated_at','DESC')
        ->get();
        
        return $query;
    }
}