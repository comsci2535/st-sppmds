<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends MX_Controller {

    private $_grpContent = 'banner';

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('slider/banner_m');
    }

    
    public function hero($page=null)
    {
        $input['type'] = $page;
        $input['active'] = '1';
        $input['recycle'] = '0';
        $info= [];
        $info = $this->banner_m->get_list($input)->result();

        $data['info'] = $info;
        $this->load->view('slider/hero', $data); 

    }

}