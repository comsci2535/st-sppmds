<section id="slider" class="slider-parallax" style="background-color: #fff;">
	<div id="oc-slider" class="owl-carousel carousel-widget" data-margin="0" data-items="1" data-pagi="false" data-loop="true" data-effect="fade" data-speed="450" data-autoplay="5000">
		<?php foreach ($info as $rs) :
			if ($rs->link): 
				?>
				<a href="#"> <?php endif; ?>
				<img src="<?php echo $rs->file ?>" alt="">
				<?php 
				if ($rs->link): 
					?>
				</a> 
			<?php 
		        endif;
		      endforeach; 
		    ?>
	</div>
</section>