"use strict";

$(document).ready(function () {
    
    $('.frm-create').validate({
        rules: {
            username: {
                remote: {
                    url: "users/check_username",
                    type: "post",
                    data: {id: function () {
                            return $('#input-id').val();
                        }, mode: function () {
                            return $('#input-mode').val();
                        }, csrfToken: csrfToken}
                }
            },
            rePassword: {equalTo: "#input-password"}
        },
        messages: {
            username: {remote: 'พบ username ซ้ำในระบบ'},
            rePassword: {equalTo: 'โปรดระบุรหัสผ่านอีกครั้ง'}
        }
    });

    $('.frm-edit').validate({
        rules: {
            username: {
                remote: {
                    url: "users/check_username",
                    type: "post",
                    data: {id: function () {
                            return $('#input-id').val();
                        }, mode: function () {
                            return $('#input-mode').val();
                        }, csrfToken: csrfToken}
                }
            }
        },
        messages: {
            username: {remote: 'พบ username ซ้ำในระบบ'}
        }
    });

    $('.frm-change-password').validate({
        rules: {
            rePassword: {equalTo: "#input-password"}
        },
        messages: {
            rePassword: {equalTo: 'โปรดระบุรหัสผ่านอีกครั้ง'}
        }
    });

    // form validate editor
    $('form').each(function () {
        if ($(this).data('validator'))
            $(this).data('validator').settings.ignore = ".note-editor *";
    });

    //fileinput;
    $('#file').fileinput({
        initialPreview: [file_image],
        initialPreviewAsData: true,
        initialPreviewConfig: [
        { downloadUrl: file_image  ,extra: {id: file_id, csrfToken: get_cookie('csrfCookie')} },
        ],
        deleteUrl: deleteUrl,
        maxFileCount: 1,
        validateInitialCount: true,
        overwriteInitial: false,
        showUpload: false,
        showRemove: true,
        required: required_icon,
        initialPreviewAsData: true,
        maxFileSize:1024,
        browseOnZoneClick: true,
        allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
        browseLabel: "Select Image",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop icon here …",
    });
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
