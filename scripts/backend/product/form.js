"use strict";

$(document).ready(function () {

  if(($(".dynamic-row").length) == 1){
      $(".dynamic-rm-bt").hide();
  }
  if(($(".dynamic-row").length) >= 3){
      $(".dynamic-add-bt").hide();
  }

  $(".dynamic-add-bt").click(function() {
    var row = $("." + $(this).data("target") + ":first").clone(true);
    var numrow = $("." + $(this).data("target")).length;
    var numrow_d = $('.dynamic-row').length;
    var target = $(this).parent();
    console.log(row)
    row.find("input").val("");
    row.find(".dynamic-rm-bt").show();
    row.insertBefore(target);

    if(numrow === 2){
      $(this).hide()
    }

});


  $(".dynamic-rm-bt").click(function() {
    var numrow = $("." + $(this).data("target")).length;
    console.log(numrow)
    if ($("." + $(this).data("target")).length > 1) {
      $(this)
      .closest(".dynamic-row")
      .remove();

      $.each($("." + $(this).data("target") + " .row-num"), function(key, value) {
        $(this).html(String(key + 1) + ".");
    });
  }
  if ($("." + $(this).data("target")).length == 1) {
      $("." + $(this).data("target") + " .dynamic-rm-bt").hide();
  }else{
      $("." + $(this).data("target") + " .dynamic-rm-bt").show();
  }
  if ($("input[name='" + $(this).data("target") + "']").hasClass("dup-group-count")) {
      dupGroupCount($(this).data("target"));
  }

  if(numrow <= 3){
      $(".dynamic-add-bt").show()
  }

  return false;
});  

  $('.frm-create').validate({
    rules: {
        title: true,
        slug:true,
        category_id: {
            required:true
        },
        price: {
            required:false
        },
        file: {
            required:true
        }
    }
});

    // form validate editor
    $('form').each(function () {
        if ($(this).data('validator'))
            $(this).data('validator').settings.ignore = ".note-editor *";
    });

    //fileinput;
    if(method == 'create'){
        $('#file').fileinput({
            maxFileCount: 5,
            validateInitialCount: true,
            overwriteInitial: true,
            showUpload: false,
            showRemove: true,
            showCancel:false,
            required: true,
            initialPreviewAsData: true,
            initialPreviewShowDelete: true,
            maxFileSize:5120,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ],
            browseLabel: "Picture",
            browseClass: "btn btn-primary btn-block",
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop picture here …",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            autoReplace: true,
        }).on('filesorted', function(e, params) {
            console.log('File sorted params', params);
        }).on('fileuploaded', function(e, params) {
            console.log('File uploaded params', params);
        });
    }
     

     if(method == 'edit'){
        $('#file').fileinput({
            initialPreview: file_product,
            initialPreviewAsData: false,
            initialPreviewConfig: download ,
            deleteUrl: deleteUrl,
            maxFileCount: 5,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            showCancle: false,
            required: required_icon,
            initialPreviewAsData: true,
            maxFileSize:1024,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
            browseLabel: "Select Image",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop icon here …",
        });
    }

    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
