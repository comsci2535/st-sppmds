<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include("view/meta.php"); ?>
	<title>ผลงานของเรา | SPPMDS</title>
</head>

<body class="stretched">
	<div id="wrapper" class="clearfix">
		<header id="header">
			<div id="header-wrap">
				<div class="container clearfix">
					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
					<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
					</div>
					<nav id="primary-menu">
						<ul>
							<li><a href="index.php"><div>หน้าแรก</div></a></li>
							<li><a href="about.php"><div>เกี่ยวกับเรา</div></a></li>
							<li><a href="shop.php"><div>สินค้า</div></a>
								<ul>
									<li><a href="#"><div>ของตกแต่งบ้าน</div></a></li>
									<li><a href="#"><div>แก้ว</div></a></li>
									<li><a href="#"><div>กระเป๋าผ้า</div></a></li>
									<li><a href="#"><div>ของเล่น</div></a></li>
									<li><a href="#"><div>ตุ๊กตา</div></a></li>
									<li><a href="#"><div>หมวก</div></a></li>
								</ul>
							</li>
							<li class="current"><a href="portfolio.php"><div>ผลงานที่ผ่านมา</div></a></li>
							<li><a href="contact.php"><div>ติดต่อเรา</div></a></li>
						</ul>
						<div id="top-search">
							<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
							<form action="search.html" method="get">
								<input type="text" name="q" class="form-control" value="" placeholder="คำที่ค้นหา ..">
							</form>
						</div>
					</nav>
				</div>
			</div>
		</header>

		<section id="page-title">
			<div class="container clearfix">
				<h1>ผลงานที่ผ่านมา</h1>
				<span>ผลงานของเราที่ผ่านมา</span>
				<ol class="breadcrumb">
					<li><a href="#">หน้าแรก</a></li>
					<li class="active">ผลงานที่ผ่านมา</li>
				</ol>
			</div>
		</section>

		<div class="clear"></div>

		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					<div class="heading-block center">
						<h3>ผลงานที่ผ่านมา</h3>
						<span></span>
					</div>
				</div>

				<div id="portfolio" class="portfolio portfolio-5 portfolio-nomargin portfolio-full portfolio-notitle clearfix" style="margin-bottom: -80px !important;">

					<article class="portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="images/portfolio/4/1.jpg" alt="Open Imagination">
							</a>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">ชื่อสินค้า</a></h3>
							<span><a href="#">หมวดสินค้า</a></span>
						</div>
					</article>

					<article class="portfolio-item pf-illustrations">
						<div class="portfolio-image">
							<a href="#">
								<img src="images/portfolio/4/2.jpg" alt="Locked Steel Gate">
							</a>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">ชื่อสินค้า</a></h3>
							<span><a href="#">หมวดสินค้า</a></span>
						</div>
					</article>

					<article class="portfolio-item pf-graphics pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="images/portfolio/4/3.jpg" alt="Mac Sunglasses">
							</a>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">ชื่อสินค้า</a></h3>
							<span><a href="#">หมวดสินค้า</a></span>
						</div>
					</article>

					<article class="portfolio-item pf-icons pf-illustrations">
						<div class="portfolio-image">
							<a href="#"><img src="images/portfolio/4/4-1.jpg" alt="Morning Dew"></a>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">ชื่อสินค้า</a></h3>
							<span><a href="#">หมวดสินค้า</a></span>
						</div>
					</article>

					<article class="portfolio-item pf-uielements pf-media">
						<div class="portfolio-image">
							<a href="#">
								<img src="images/portfolio/4/5.jpg" alt="Console Activity">
							</a>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">ชื่อสินค้า</a></h3>
							<span><a href="#">หมวดสินค้า</a></span>
						</div>
					</article>

				</div>

			</div>
		</section>

		<?php include("view/footer.php"); ?>

	</div>

	<?php include("view/script.php"); ?>

</body>
</html>
