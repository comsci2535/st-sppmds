<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include("view/meta.php"); ?>
	<title>สินค้า - Shop | SPPMDS</title>
</head>

<body class="stretched">
	<div id="wrapper" class="clearfix">

		<header id="header">
			<div id="header-wrap">
				<div class="container clearfix">
					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
					<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
					</div>
					<nav id="primary-menu">
						<ul>
							<li><a href="index.php"><div>หน้าแรก</div></a></li>
							<li><a href="about.php"><div>เกี่ยวกับเรา</div></a></li>
							<li class="current"><a href="shop.php"><div>สินค้า</div></a>
								<ul>
									<li><a href="#"><div>ของตกแต่งบ้าน</div></a></li>
									<li><a href="#"><div>แก้ว</div></a></li>
									<li><a href="#"><div>กระเป๋าผ้า</div></a></li>
									<li><a href="#"><div>ของเล่น</div></a></li>
									<li><a href="#"><div>ตุ๊กตา</div></a></li>
									<li><a href="#"><div>หมวก</div></a></li>
								</ul>
							</li>
							<li><a href="portfolio.php"><div>ผลงานที่ผ่านมา</div></a></li>
							<li><a href="contact.php"><div>ติดต่อเรา</div></a></li>
						</ul>
						<div id="top-search">
							<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
							<form action="search.html" method="get">
								<input type="text" name="q" class="form-control" value="" placeholder="คำที่ค้นหา ..">
							</form>
						</div>
					</nav>
				</div>
			</div>
		</header>

		<section id="page-title">
			<div class="container clearfix">
				<h1>สินค้า</h1>
				<span>เลือกสินค้าตามใจคุณ</span>
				<ol class="breadcrumb">
					<li><a href="#">หน้าแรก</a></li>
					<li class="active">สินค้า</li>
				</ol>
			</div>
		</section>

		<div class="clear"></div>

		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
					<div class="postcontent nobottommargin col_last">

						<div id="shop" class="shop product-3 clearfix">
							<div class="product clearfix">
								<div class="product-image">
									<a href="detailshop.php"><img src="images/shop/dress/1.jpg" alt="Checked Short Dress"></a>
									<div class="sale-flash">50% Off*</div>
									<div class="product-overlay">
										<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
										<a href="images/shop/dress/1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
									</div>
								</div>
								<div class="product-desc center">
									<div class="product-title"><h3><a href="detailshop.php">Checked Short Dress</a></h3></div>
									<div class="product-price"><del>฿ 24.99</del> <ins>฿ 12.49</ins></div>
								</div>
							</div>

							<div class="product clearfix">
								<div class="product-image">
									<a href="detailshop.php"><img src="images/shop/pants/1-1.jpg" alt="Slim Fit Chinos"></a>
									<div class="product-overlay">
										<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
										<a href="images/shop/pants/1-1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
									</div>
								</div>
								<div class="product-desc center">
									<div class="product-title"><h3><a href="detailshop.php">Slim Fit Chinos</a></h3></div>
									<div class="product-price">฿ 39.99</div>
								</div>
							</div>

							<div class="product clearfix">
								<div class="product-image">
									<a href="detailshop.php"><img src="images/shop/shoes/1.jpg" alt="Dark Brown Boots"></a>
									<div class="product-overlay">
										<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
										<a href="images/shop/shoes/1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
									</div>
								</div>
								<div class="product-desc center">
									<div class="product-title"><h3><a href="detailshop.php">Dark Brown Boots</a></h3></div>
									<div class="product-price">฿ 39.99</div>
								</div>
							</div>

							<div class="product clearfix">
								<div class="product-image">
									<a href="detailshop.php"><img src="images/shop/dress/2.jpg" alt="Light Blue Denim Dress"></a>
									<div class="product-overlay">
										<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
										<a href="images/shop/dress/2.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
									</div>
								</div>
								<div class="product-desc center">
									<div class="product-title"><h3><a href="detailshop.php">Light Blue Denim Dress</a></h3></div>
									<div class="product-price">฿ 19.95</div>
								</div>
							</div>

							<div class="product clearfix">
								<div class="product-image">
									<a href="detailshop.php"><img src="images/shop/sunglasses/1.jpg" alt="Unisex Sunglasses"></a>
									<div class="sale-flash">Sale!</div>
									<div class="product-overlay">
										<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
										<a href="images/shop/sunglasses/1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
									</div>
								</div>
								<div class="product-desc center">
									<div class="product-title"><h3><a href="detailshop.php">Unisex Sunglasses</a></h3></div>
									<div class="product-price"><del>฿ 19.99</del> <ins>฿ 11.99</ins></div>
								</div>
							</div>

							<div class="product clearfix">
								<div class="product-image">
									<a href="detailshop.php"><img src="images/shop/tshirts/1.jpg" alt="Blue Round-Neck Tshirt"></a>
									<div class="product-overlay">
										<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
										<a href="images/shop/tshirts/1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
									</div>
								</div>
								<div class="product-desc center">
									<div class="product-title"><h3><a href="detailshop.php">Blue Round-Neck Tshirt</a></h3></div>
									<div class="product-price">฿ 9.99</div>
								</div>
							</div>

							<div class="product clearfix">
								<div class="product-image">
									<a href="detailshop.php"><img src="images/shop/watches/1.jpg" alt="Silver Chrome Watch"></a>
									<div class="product-overlay">
										<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
										<a href="images/shop/watches/1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
									</div>
								</div>
								<div class="product-desc center">
									<div class="product-title"><h3><a href="detailshop.php">Silver Chrome Watch</a></h3></div>
									<div class="product-price">฿ 129.99</div>
								</div>
							</div>

							<div class="product clearfix">
								<div class="product-image">
									<a href="detailshop.php"><img src="images/shop/shoes/2.jpg" alt="Men Grey Casual Shoes"></a>
									<div class="sale-flash">Sale!</div>
									<div class="product-overlay">
										<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
										<a href="images/shop/shoes/2.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
									</div>
								</div>
								<div class="product-desc center">
									<div class="product-title"><h3><a href="detailshop.php">Men Grey Casual Shoes</a></h3></div>
									<div class="product-price"><del>฿ 45.99</del> <ins>฿ 39.49</ins></div>
								</div>
							</div>

							<div class="product clearfix">
								<div class="product-image">
									<a href="detailshop.php"><img src="images/shop/dress/3.jpg" alt="Pink Printed Dress"></a>
									<div class="sale-flash">Sale!</div>
									<div class="product-overlay">
										<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
										<a href="images/shop/dress/3.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
									</div>
								</div>
								<div class="product-desc center">
									<div class="product-title"><h3><a href="detailshop.php">Pink Printed Dress</a></h3></div>
									<div class="product-price"><del>฿ 45.99</del> <ins>฿ 39.49</ins></div>
								</div>
							</div>

							<div class="product clearfix">
								<div class="product-image">
									<a href="detailshop.php"><img src="images/shop/pants/5.jpg" alt="Green Trousers"></a>
									<div class="sale-flash">Sale!</div>
									<div class="product-overlay">
										<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
										<a href="images/shop/pants/5.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
									</div>
								</div>
								<div class="product-desc center">
									<div class="product-title"><h3><a href="detailshop.php">Green Trousers</a></h3></div>
									<div class="product-price"><del>฿ 24.99</del> <ins>฿ 21.99</ins></div>
								</div>
							</div>

							<div class="product clearfix">
								<div class="product-image">
									<a href="detailshop.php"><img src="images/shop/sunglasses/2.jpg" alt="Men Aviator Sunglasses"></a>
									<div class="product-overlay">
										<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
										<a href="images/shop/sunglasses/2.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
									</div>
								</div>
								<div class="product-desc center">
									<div class="product-title"><h3><a href="detailshop.php">Men Aviator Sunglasses</a></h3></div>
									<div class="product-price">฿ 13.49</div>
								</div>
							</div>

							<div class="product clearfix">
								<div class="product-image">
									<a href="detailshop.php"><img src="images/shop/tshirts/4.jpg" alt="Black Polo Tshirt"></a>
									<div class="product-overlay">
										<a href="detailshop.php" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
										<a href="images/shop/tshirts/4.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
									</div>
								</div>
								<div class="product-desc center">
									<div class="product-title"><h3><a href="detailshop.php">Black Polo Tshirt</a></h3></div>
									<div class="product-price">฿ 11.49</div>
								</div>
							</div>

						</div>
					</div>

					<div class="sidebar nobottommargin">
						<div class="sidebar-widgets-wrap">

							<div class="widget widget_links clearfix">
								<h4>หมวดสินค้า</h4>
								<ul>
									<li><a href="#"><div>ของตกแต่งบ้าน</div></a></li>
									<li><a href="#"><div>แก้ว</div></a></li>
									<li><a href="#"><div>กระเป๋าผ้า</div></a></li>
									<li><a href="#"><div>ของเล่น</div></a></li>
									<li><a href="#"><div>ตุ๊กตา</div></a></li>
									<li><a href="#"><div>หมวก</div></a></li>
								</ul>
							</div>

							<div class="widget clearfix">
								<h4>สินค้าแนะนำ</h4>
								<div id="post-list-footer">
									<div class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="images/shop/small/1.jpg" alt="Image"></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Blue Round-Neck Tshirt</a></h4>
											</div>
											<ul class="entry-meta">
												<li class="color">฿ 29.99</li>
											</ul>
										</div>
									</div>
									<div class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="images/shop/small/6.jpg" alt="Image"></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Checked Short Dress</a></h4>
											</div>
											<ul class="entry-meta">
												<li class="color">฿ 23.99</li>
											</ul>
										</div>
									</div>
									<div class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="images/shop/small/7.jpg" alt="Image"></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Light Blue Denim Dress</a></h4>
											</div>
											<ul class="entry-meta">
												<li class="color">฿ 19.99</li>
											</ul>
										</div>
									</div>
								</div>
							</div>

							<div class="widget clearfix">
								<h4>สินค้าในหมวดเดียวกัน</h4>
								<div class="widget-last-view">
									<div class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="images/shop/small/3.jpg" alt="Image"></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Round-Neck Tshirt</a></h4>
											</div>
											<ul class="entry-meta">
												<li class="color">฿ 15</li>
											</ul>
										</div>
									</div>
									<div class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="images/shop/small/10.jpg" alt="Image"></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Green Trousers</a></h4>
											</div>
											<ul class="entry-meta">
												<li class="color">฿ 19</li>
											</ul>
										</div>
									</div>
									<div class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="images/shop/small/11.jpg" alt="Image"></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Silver Chrome Watch</a></h4>
											</div>
											<ul class="entry-meta">
												<li class="color">฿ 34.99</li>
											</ul>
										</div>
									</div>
								</div>
							</div>

							<div class="widget clearfix">
								<div id="fb-root"></div>
								<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v3.2&appId=289137874657&autoLogAppEvents=1"></script>
								<div class="fb-page" data-href="https://www.facebook.com/SakidloCode/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/SakidloCode/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/SakidloCode/">SakidloCode</a></blockquote></div>
							</div>

							<div class="widget clearfix">
								<div id="oc-clients-full" class="owl-carousel image-carousel carousel-widget" data-items="1" data-margin="10" data-loop="true" data-nav="false" data-autoplay="5000" data-pagi="false">
									<div class="oc-item"><a href="#"><img src="images/clients/1.png" alt="Clients"></a></div>
									<div class="oc-item"><a href="#"><img src="images/clients/2.png" alt="Clients"></a></div>
									<div class="oc-item"><a href="#"><img src="images/clients/3.png" alt="Clients"></a></div>
									<div class="oc-item"><a href="#"><img src="images/clients/4.png" alt="Clients"></a></div>
									<div class="oc-item"><a href="#"><img src="images/clients/5.png" alt="Clients"></a></div>
									<div class="oc-item"><a href="#"><img src="images/clients/6.png" alt="Clients"></a></div>
									<div class="oc-item"><a href="#"><img src="images/clients/7.png" alt="Clients"></a></div>
									<div class="oc-item"><a href="#"><img src="images/clients/8.png" alt="Clients"></a></div>
								</div>
							</div>
						</div>

					</div><!-- .sidebar end -->
				</div>
			</div>
		</section>

		<?php include("view/footer.php"); ?>

	</div>

	<?php include("view/script.php"); ?>

</body>
</html>
