<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>
        <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal  frm-main', 'method' => 'post')) ?>
        <div class="m-portlet__body">
             <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" > <h4 class="block">Administrator email</h4></label>
                <div class="col-sm-7"></div>
            </div>  
           
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" >Default</label>
                <div class="col-sm-7">
                    <input value="<?php echo isset($info['mailDefault']) ? $info['mailDefault'] : NULL ?>" type="text" id="" class="form-control" name="mailDefault">
                </div>
            </div>   
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" > <h4 class="block">Mail sever</h4></label>
                <div class="col-sm-7"></div>
            </div>    
            <div class="form-group m-form__group row ">
                <label class="col-sm-2 col-form-label" >Send method</label>
                <div class="col-sm-7 icheck-inline">
                    <?php  $info['mailMethod'] = isset($info['mailMethod']) ? $info['mailMethod'] : 1 ?>
                    <label><input <?php echo $info['mailMethod']==1 ? "checked" : NULL; ?> type="radio" name="mailMethod" class="icheck" value="1"> SMTP</label>
                    <label><input <?php echo $info['mailMethod']==2 ? "checked" : NULL; ?> type="radio" name="mailMethod" class="icheck" value="2"> PHP mail function</label>
                </div>
            </div>    
            <div class="form-group m-form__group row ">
                <label class="col-sm-2 col-form-label" >SMTP Authenticate</label>
                <div class="col-sm-7 icheck-inline">
                    <?php  $info['mailAuthenticate'] = isset($info['mailAuthenticate']) ? $info['mailAuthenticate'] : 1 ?>
                    <label><input  <?php echo $info['mailAuthenticate']==1 ? "checked" : NULL; ?> type="radio" name="mailAuthenticate" class="icheck" value="1" checked> Yes</label>
                    <label><input  <?php echo $info['mailAuthenticate']==2 ? "checked" : NULL; ?> type="radio" name="mailAuthenticate" class="icheck" value="2"> No</label>
                </div>
            </div>                  
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" >SMTP Sever</label>
                <div class="col-sm-7">
                    <input value="<?php echo isset($info['SMTPserver']) ? $info['SMTPserver'] : NULL; ?>" type="text" id="" class="form-control" name="SMTPserver">
                </div>
            </div> 
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" >SMTP Port</label>
                <div class="col-sm-7">
                    <input value="<?php echo isset($info['SMTPport']) ? $info['SMTPport'] : NULL; ?>" type="text" id="" class="form-control" name="SMTPport">
                </div>
            </div>     
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" >SMTP username</label>
                <div class="col-sm-7">
                    <input value="<?php echo isset($info['SMTPusername']) ? $info['SMTPusername'] : NULL; ?>" type="text" id="" class="form-control" name="SMTPusername">
                </div>
            </div>       
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" >SMTP password</label>
                <div class="col-sm-7">
                    <input value="<?php echo isset($info['SMTPpassword']) ? $info['SMTPpassword'] : NULL; ?>" type="password" id="" class="form-control" name="SMTPpassword">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" > <h4 class="block">Sender mail</h4></label>
                <div class="col-sm-7"></div>
            </div>          
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" >Sender name</label>
                <div class="col-sm-7">
                    <input value="<?php echo isset($info['senderName']) ? $info['senderName'] : NULL; ?>" type="text" id="" class="form-control" name="senderName">
                </div>
            </div>     
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" >Sender Email</label>
                <div class="col-sm-7">
                    <input value="<?php echo isset($info['senderEmail']) ? $info['senderEmail'] : NULL; ?>" type="text" id="" class="form-control" name="senderEmail">
                </div>
            </div>                  
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">

            <div class="m-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <button type="submit" class="btn btn-primary pullleft">บันทึก</button>
                <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                    </div>
                </div>
                
            </div>
        </div>
        
        <?php echo form_close() ?>

    </div>  
</div>
