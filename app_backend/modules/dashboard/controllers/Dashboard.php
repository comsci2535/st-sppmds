<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Dashboard extends MX_Controller {

    

    private $_title = 'แผงควบคุม';

    private $_pageExcerpt = 'แสดงรายการโดยรวมของระบบ';

    private $_grpContent = 'grpContent';

    

    public function __construct() {

        parent::__construct();

        $this->load->model("report_website/report_website_m");

        

    }



    public function index() {

        $this->load->module('template');

        $data['ddYear'] = $this->report_website_m->get_year_dd();
        $data['ddMonth'] = $this->report_website_m->get_month_dd();

        if ( $this->session->firstTime )

            Modules::run('utils/toastr', 'info', config_item('appName'), 'ยินดีต้อนรับ');   

      

        

        // page detail

        $data['pageHeader'] = $this->_title;

        $data['pageExcerpt'] = $this->_pageExcerpt;

        $data['contentView'] = "{$this->router->class}/index";

             

        $this->template->layout($data);

    }



    

}

