<div class="col-md-12">

    <div class="m-portlet m-portlet--tab">

        <div class="m-portlet__head">

            <div class="m-portlet__head-caption">

                <div class="m-portlet__head-title">

                    <h3 class="m-portlet__head-text">

                     แบบฟอร์ม

                 </h3>

             </div>

         </div>

         <div class="m-portlet__head-tools">



         </div>

     </div>





     <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>

     <div class="m-portlet__body">



        <div class="form-group m-form__group row">

            <label class="col-sm-2 col-form-label" for="title">หัวข้อ</label>

            <div class="col-sm-7">

                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input m-input--square" name="title" id="title" required>

                <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>

            </div>

        </div>

        <div class="form-group m-form__group row">

            <label class="col-sm-2 col-form-label" for="slug">Slug</label>

            <div class="col-sm-7">

                <input value="<?php echo isset($info->slug) ? $info->slug : NULL ?>" type="text" class="form-control m-input m-input--square" name="slug" id="slug" readonly required>

                <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>

            </div>

        </div>

        <div class="form-group m-form__group row">

            <label class="col-sm-2 col-form-label" for="code">รหัสสินค้า</label>

            <div class="col-sm-7">

                <input value="<?php echo isset($info->code) ? $info->code : NULL ?>" type="text" class="form-control m-input m-input--square" name="code" id="code" required>


            </div>

        </div>



        <div class="form-group m-form__group row">

            <label class="col-sm-2 col-form-label" for="slug">หมวดสินค้า</label>

            <div class="col-sm-7">

                <select id="category_id" name="category_id" class="form-control m-input m-input--square" required>

                    <option value="">เลือก</option>

                    <?php

                    if(isset($infotype)):

                        $selected = '';

                        foreach($infotype as $item):

                            if($info->category_id == $item->category_id):

                                $selected = 'selected';

                            else:

                                $selected = '';

                            endif;



                            ?>

                            <option value="<?=$item->category_id;?>" <?=$selected?>><?=$item->title;?></option>

                            <?php

                        endforeach;



                    endif;

                    ?>



                </select>

            </div>

        </div>



        <div class="form-group m-form__group row">

            <label class="col-sm-2 col-form-label" for="file">ไฟล์</label>

            <div class="col-sm-7">

                <input id="file" name="file[]" type="file"  data-preview-file-type="text" multiple accept="image/*">

                <input type="hidden" name="outfile" value="<?=(isset($info->file)) ? $info->file : ''; ?>">

            </div>

        </div> 



        <div class="form-group m-form__group row">

            <label class="col-sm-2 col-form-label" for="title">รายละเอียดย่อ</label>

            <div class="col-sm-7">

                <textarea name="excerpt" rows="3" class="form-control " id="excerpt" required><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>

            </div>

        </div>

        <div class="form-group m-form__group row">

            <label class="col-sm-2 col-form-label" for="title">ราคาหลัก</label>

            <div class="col-sm-7">

                <input type="number" name="price" placeholder="" class="form-control " id="price"  value="<?php echo isset($info->price) ? $info->price : NULL ?>">

            </div>

        </div>

        <div class="form-group m-form__group row">

            <label class="col-sm-2 col-form-label" for="title">ลดราคา ( % )</label>

            <div class="col-sm-7">

                <input type="number" name="sell" placeholder="" class="form-control " id="sell"  value="<?php echo isset($info->sell) ? $info->sell : NULL ?>">

            </div>

        </div>

        <div class="form-group m-form__group row">

            <label class="col-sm-2 col-form-label" for="title">SKU</label>

            <div class="col-sm-7">

                <input type="text" name="sku" placeholder="" class="form-control " id="sku"  value="<?php echo isset($info->sku) ? $info->sku : NULL ?>">

            </div>

        </div>



        <?php echo $this->load->view('feature');?>



        <div class="form-group m-form__group row">

            <label class="col-sm-2 col-form-label" for="detail">รายละเอียด</label>

            <div class="col-sm-7">

                <textarea name="detail" rows="3" class="form-control summernote" id="detail" required><?php echo isset($info->detail) ? $info->detail : NULL ?></textarea>

            </div>

        </div>  

    </div>

    <div class="m-portlet__foot m-portlet__foot--fit">



        <div class="m-form__actions">

            <div class="row">

                <div class="col-2">

                </div>

                <div class="col-10">

                    <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>

                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->

                </div>

            </div>



        </div>

    </div>

    <?php 

    $csrf = array(

        'name' => $this->security->get_csrf_token_name(),

        'hash' => $this->security->get_csrf_hash()

    );

    ?>

    <input type="hidden" id="<?=$csrf['name'];?>" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />

    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">

    <input type="hidden" class="form-control" name="db" id="db" value="product">

    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->product_id) ? encode_id($info->product_id) : 0 ?>">

    <?php echo form_close() ?>



    <!--end::Form-->

</div>





</div>



<script>



    //set par fileinput;

    var file_product = []; 

    var required_icon   = true; 

    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : ''; ?>';

    var file_id         = '<?=$info->img_id;?>';

    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile';

    var download = [];



    <?php foreach ($imgs as $img ) {   

        ?>

        file_product.push('<?=$this->config->item('root_url').$img->file;?>');

    <?php } ?>



    <?php foreach ($imgs as $img ) {   

        ?>

        download.push({

            downloadUrl : '<?=$this->config->item('root_url').$img->file;?>' ,

            key : <?=$img->img_id?>,

            extra:{

                id: <?=$img->img_id?>,

                csrfToken : '<?=$csrf['hash'];?>'

            }

        });

    <?php } ?>



</script>









