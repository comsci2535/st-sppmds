<br>
<hr>
<h4 class="pl-5">คุณสมบัติ</h4>
<br>
<?php 
if($this->router->method == 'edit'){
	if(sizeof($features) > 0){
		foreach ($features as $feature) {
			?>
			<div class="feature-data-edit dynamic-row">
				<div class="form-group m-form__group row">
					<label class="col-sm-2 col-form-label" for="title">ลักษณะเฉพาะ</label>
					<div class="col-sm-7">
						<input type="hidden" name="feature_id[]" value="<?=$feature->feature_id?>">
						<input type="text" name="feature_name_edit[]" placeholder="ตัวอย่าง : ขนาดไซส์ x" class="form-control " id="feature_name"  value="<?php echo isset($feature->feature_name) ? $feature->feature_name : NULL ?>">
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-sm-2 col-form-label" for="title">รายละเอียด 1</label>
					<div class="col-sm-7">
						<input type="text" name="feature_dis_edit1[]" placeholder="ตัวอย่าง : 1-50 ชิ้น" class="form-control " id="feature_dis"  value="<?php echo isset($feature->feature_dis1) ? $feature->feature_dis1 : NULL ?>">
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-sm-2 col-form-label" for="title">ราคาเฉพาะ 1</label>
					<div class="col-sm-7">
						<input type="number" name="feature_price_edit1[]"  placeholder="ตัวอย่าง : 1200" class="form-control " id="feature_price"  value="<?php echo isset($feature->feature_price1) ? $feature->feature_price1 : NULL ?>">
					</div>
				</div><div class="form-group m-form__group row">
					<label class="col-sm-2 col-form-label" for="title">รายละเอียด 2</label>
					<div class="col-sm-7">
						<input type="text" name="feature_dis_edit2[]" placeholder="ตัวอย่าง : 1-50 ชิ้น" class="form-control " id="feature_dis"  value="<?php echo isset($feature->feature_dis2) ? $feature->feature_dis2 : NULL ?>">
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-sm-2 col-form-label" for="title">ราคาเฉพาะ 2</label>
					<div class="col-sm-7">
						<input type="number" name="feature_price_edit2[]"  placeholder="ตัวอย่าง : 1200" class="form-control " id="feature_price"  value="<?php echo isset($feature->feature_price2) ? $feature->feature_price2 : NULL ?>">
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-sm-2 col-form-label" for="title">รายละเอียด 3</label>
					<div class="col-sm-7">
						<input type="text" name="feature_dis_edit3[]" placeholder="ตัวอย่าง : 1-50 ชิ้น" class="form-control " id="feature_dis"  value="<?php echo isset($feature->feature_dis3) ? $feature->feature_dis3 : NULL ?>">
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-sm-2 col-form-label" for="title">ราคาเฉพาะ 3</label>
					<div class="col-sm-7">
						<input type="number" name="feature_price_edit3[]"  placeholder="ตัวอย่าง : 1200" class="form-control " id="feature_price"  value="<?php echo isset($feature->feature_price3) ? $feature->feature_price3 : NULL ?>">
					</div>
				</div>
				<div class=" btn btn-danger delete-row dynamic-rm-bt" data-target="feature-data-edit" style="margin-top: 10px; margin-bottom: 10px; margin-left: 70%;">
					<i class="fa flaticon-delete"></i>
				</div>
				<hr>
			</div>
			<?php 
		}
	}
	?>
	<div class="feature-data dynamic-row">
		<div class="form-group m-form__group row">
			<label class="col-sm-2 col-form-label" for="title">ลักษณะเฉพาะ</label>
			<div class="col-sm-7">
				<input type="text" name="feature_name[]" placeholder="ตัวอย่าง : ขนาดไซส์ x" class="form-control " id="feature_name"  value="">
			</div>
		</div>
		<div class="form-group m-form__group row">
			<label class="col-sm-2 col-form-label" for="title">รายละเอียด 1</label>
			<div class="col-sm-7">
				<input type="text" name="feature_dis1[]" placeholder="ตัวอย่าง : 1-50 ชิ้น" class="form-control " id="feature_dis"  value="">
			</div>
		</div>
		<div class="form-group m-form__group row">
			<label class="col-sm-2 col-form-label" for="title">ราคาเฉพาะ 2</label>
			<div class="col-sm-7">
				<input type="number" name="feature_price1[]"  placeholder="ตัวอย่าง : 1200" class="form-control " id="feature_price"  value="">
			</div>
		</div>
		<div class="form-group m-form__group row">
			<label class="col-sm-2 col-form-label" for="title">รายละเอียด 2</label>
			<div class="col-sm-7">
				<input type="text" name="feature_dis2[]" placeholder="ตัวอย่าง : 1-50 ชิ้น" class="form-control " id="feature_dis"  value="">
			</div>
		</div>
		<div class="form-group m-form__group row">
			<label class="col-sm-2 col-form-label" for="title">ราคาเฉพาะ 2</label>
			<div class="col-sm-7">
				<input type="number" name="feature_price2[]"  placeholder="ตัวอย่าง : 1200" class="form-control " id="feature_price"  value="">
			</div>
		</div><div class="form-group m-form__group row">
			<label class="col-sm-2 col-form-label" for="title">รายละเอียด 3</label>
			<div class="col-sm-7">
				<input type="text" name="feature_dis3[]" placeholder="ตัวอย่าง : 1-50 ชิ้น" class="form-control " id="feature_dis"  value="">
			</div>
		</div>
		<div class="form-group m-form__group row">
			<label class="col-sm-2 col-form-label" for="title">ราคาเฉพาะ 3</label>
			<div class="col-sm-7">
				<input type="number" name="feature_price3[]"  placeholder="ตัวอย่าง : 1200" class="form-control " id="feature_price"  value="">
			</div>
		</div>
		<div class=" btn btn-danger delete-row dynamic-rm-bt" data-target="feature-data" style="margin-top: 10px; margin-bottom: 10px; margin-left: 70%;">
			<i class="fa flaticon-delete"></i>
		</div>
		<hr>
	</div>
	<?php
}else{
	?>
	<div class="feature-data dynamic-row">
		<div class="form-group m-form__group row">
			<label class="col-sm-2 col-form-label" for="title">ลักษณะเฉพาะ</label>
			<div class="col-sm-7">
				<input type="text" name="feature_name[]" placeholder="ตัวอย่าง : ขนาดไซส์ x" class="form-control " id="feature_name"  value="">
			</div>
		</div>
		<div class="form-group m-form__group row">
			<label class="col-sm-2 col-form-label" for="title">รายละเอียด 1</label>
			<div class="col-sm-7">
				<input type="text" name="feature_dis1[]" placeholder="ตัวอย่าง : 1-50 ชิ้น" class="form-control " id="feature_dis"  value="">
			</div>
		</div>
		<div class="form-group m-form__group row">
			<label class="col-sm-2 col-form-label" for="title">ราคาเฉพาะ 2</label>
			<div class="col-sm-7">
				<input type="number" name="feature_price1[]"  placeholder="ตัวอย่าง : 1200" class="form-control " id="feature_price"  value="">
			</div>
		</div>
		<div class="form-group m-form__group row">
			<label class="col-sm-2 col-form-label" for="title">รายละเอียด 2</label>
			<div class="col-sm-7">
				<input type="text" name="feature_dis2[]" placeholder="ตัวอย่าง : 1-50 ชิ้น" class="form-control " id="feature_dis"  value="">
			</div>
		</div>
		<div class="form-group m-form__group row">
			<label class="col-sm-2 col-form-label" for="title">ราคาเฉพาะ 2</label>
			<div class="col-sm-7">
				<input type="number" name="feature_price2[]"  placeholder="ตัวอย่าง : 1200" class="form-control " id="feature_price"  value="">
			</div>
		</div><div class="form-group m-form__group row">
			<label class="col-sm-2 col-form-label" for="title">รายละเอียด 3</label>
			<div class="col-sm-7">
				<input type="text" name="feature_dis3[]" placeholder="ตัวอย่าง : 1-50 ชิ้น" class="form-control " id="feature_dis"  value="">
			</div>
		</div>
		<div class="form-group m-form__group row">
			<label class="col-sm-2 col-form-label" for="title">ราคาเฉพาะ 3</label>
			<div class="col-sm-7">
				<input type="number" name="feature_price3[]"  placeholder="ตัวอย่าง : 1200" class="form-control " id="feature_price"  value="">
			</div>
		</div>
		<div class=" btn btn-danger delete-row dynamic-rm-bt" data-target="feature-data" style="margin-top: 10px; margin-bottom: 10px; margin-left: 70%;">
			<i class="fa flaticon-delete"></i>
		</div>
		<hr>
	</div>
	<?php 
}
?>
<div class="form-group m-form__group row">
	<label class="col-sm-3 col-form-label" for="title"></label>
	<div class="btn btn-success dynamic-add-bt" data-target="feature-data">+ เพิ่ม feature ใหม่</div>
</div>
<hr>
