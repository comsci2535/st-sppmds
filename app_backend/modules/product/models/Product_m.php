<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
        ->select('a.*')
        ->from('product a')
        ->get();

        return $query;
    }

    public function get_rows_product($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
        ->select('a.*,m.file,m.img_id')
        ->from('product a')
        ->join('product_img m','a.product_id = m.product_id','left')
        ->get();

        return $query;
    }

    public function get_rows_feature($param)
    {
     $this->_condition($param);

     if ( isset($param['length']) ) 
        $this->db->limit($param['length'], $param['start']);

    $query = $this->db
    ->select('a.*,f.*')
    ->from('product a')
    ->join('product_feature f','a.product_id = f.product_id','left')
    ->get();

    return $query;
}

public function get_count($param) 
{
    $this->_condition($param);
    $query = $this->db
    ->select('a.*')
    ->from('product a')
    ->get();
    return $query->num_rows();
}

private function _condition($param) 
{   
        // START form filter 
    if ( isset($param['keyword']) && $param['keyword'] != "" ) {
        $this->db
        ->group_start()
        ->like('a.title', $param['keyword'])
        ->or_like('a.excerpt', $param['keyword'])
        ->or_like('a.detail', $param['keyword'])
        ->group_end();
    }
    if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
        $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
    }
    if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
        $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
    }     
    if ( isset($param['active']) && $param['active'] != "" ) {
        $this->db->where('a.active', $param['active']);
    }         
        // END form filter

    if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
        $this->db
        ->group_start()
        ->like('a.title', $param['search']['value'])
        ->or_like('a.excerpt', $param['search']['value'])
        ->or_like('a.detail', $param['search']['value'])
        ->group_end();
    }

    if ( isset($param['order']) ){
        if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
        if ($param['order'][0]['column'] == 2) $columnOrder = "a.excerpt";            
        if ( $this->router->method =="data_index" ) {
            if ($param['order'][0]['column'] == 3) $columnOrder = "a.created_at";
            if ($param['order'][0]['column'] == 4) $columnOrder = "a.updated_at";
        } else if ( $this->router->method =="data_trash" ) {
            if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
        }
        $this->db
        ->order_by($columnOrder, $param['order'][0]['dir']);
    } 

    if ( isset($param['product_id']) ) 
        $this->db->where('a.product_id', $param['product_id']);

    if ( isset($param['recycle']) )
        $this->db->where('a.recycle', $param['recycle']);
    

}

public function insert($value) {
    $this->db->insert('product', $value);
    return $this->db->insert_id();
}

public function insert_batch_img($id,$value) {
    $value_arr = array();
    foreach($value['files'] as $item ){
        array_push($value_arr,
            array(
                'product_id'            => $id
                ,'file'                 => $item
            )
        );
    }
    $this->db->insert_batch('product_img', $value_arr);
    return $this->db->insert_id();
}

public function insert_batch_feature($id,$value) {
    $value_arr = array();

    for ($i=0; $i < sizeof($value['feature_name']); $i++) { 
        array_push($value_arr,
            array(
                'product_id'          => $id,
                'feature_name'        => $value['feature_name'][$i],
                'feature_dis1'         => $value['feature_dis1'][$i],
                'feature_price1'       => $value['feature_price1'][$i],
                'feature_dis2'         => $value['feature_dis2'][$i],
                'feature_price2'       => $value['feature_price2'][$i],
                'feature_dis3'         => $value['feature_dis3'][$i],
                'feature_price3'       => $value['feature_price3'][$i],
            )
        );
    }
    $this->db->insert_batch('product_feature', $value_arr);
    return $this->db->insert_id();
}

public function update_batch_feature($id,$value) {
    $value_arr = array();

    for ($i=0; $i < sizeof($value['feature_id']); $i++) { 
        array_push($value_arr,
            array(
                'product_id'          => $id,
                'feature_id'          => $value['feature_id'][$i],
                'feature_name'        => $value['feature_name'][$i],
                'feature_dis1'         => $value['feature_dis1'][$i],
                'feature_price1'       => $value['feature_price1'][$i],
                'feature_dis2'         => $value['feature_dis2'][$i],
                'feature_price2'       => $value['feature_price2'][$i],
                'feature_dis3'         => $value['feature_dis3'][$i],
                'feature_price3'       => $value['feature_price3'][$i],
            )
        );
    }

    foreach ($value_arr as $value) {
        $query = $this->db
        ->where('product_id',$id)
        ->where('feature_id', $value['feature_id'])
        ->update('product_feature',$value );
    }

    return  true;
}



public function update($id, $value)
{
    $query = $this->db
    ->where('product_id', $id)
    ->update('product', $value);
    return $query;
}

public function update_in($id, $value)
{
    $query = $this->db
    ->where_in('product_id', $id)
    ->update('product', $value);
    return $query;
} 

public function delete_img($id)
{
   $query = $this->db->where('img_id', $id)->delete('product_img');
   return $query;
}

public function delete_feature($id)
{
   $query = $this->db->where('feature_id', $id)->delete('product_feature');
   return $query;
}    

}
