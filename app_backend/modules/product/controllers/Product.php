<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Product extends MX_Controller {

    private $_title = "สินค้า";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับ ___สินค้า";
    private $_grpContent = "product";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("product_m");
        $this->load->model("category/category_m");
        $this->load->library('uploadfile_library');
    }
    
    public function index(){
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf' => site_url("{$this->router->class}/pdf"),
        );
        $action[1][] = action_refresh(site_url("{$this->router->class}"));
       // $action[1][] = action_filter();
        $action[1][] = action_add(site_url("{$this->router->class}/create"));
        //$action[2][] = action_export_group($export);
        $action[3][] = action_trash_multi("{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(site_url("{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index(){
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->product_m->get_rows($input);
        $infoCount = $this->product_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->product_id);
            $action = array();
            $action[1][] = table_edit(site_url("{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['excerpt'] = $rs->excerpt;
            $column[$key]['active'] = toggle_active($active, "{$this->router->class}/action/active");
            $column[$key]['created_at'] = datetime_table($rs->created_at);
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
    
    public function create(){
        $this->load->module('template');

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/storage");

        //
        $input['active'] = 1;
        $data['infotype'] = $this->category_m->get_rows($input)->result();      
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        //$data['pageScript'] = "scripts/backend/{$this->router->class}/feature.js";
        
        $this->template->layout($data);
    }
    
    public function storage(){
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $value_file = $this->_build_datas($input);
        $value_feature = $this->_build_datas_feature($input);

        $result = $this->product_m->insert($value);

        if ( $result ) {
            //insert image prioduct
            $this->product_m->insert_batch_img($result,$value_file);
            $this->product_m->insert_batch_feature($result,$value_feature);

            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
         Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
     }
     redirect(site_url("{$this->router->class}"));
 }

 public function edit($id=""){
    $this->load->module('template');

    $id = decode_id($id);
    $input['product_id'] = $id;
    $info = $this->product_m->get_rows($input);
    $product_image = $this->product_m->get_rows_product($input);
    $product_feature    = $this->product_m->get_rows_feature($input);
    
    if ( $info->num_rows() == 0) {
        Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
        redirect_back();
    }
    $img = $product_image->result();
    $features = $product_feature->result();
    $info = $info->row();

    $data['infotype'] = $this->category_m->get_rows($input)->result();  
    $data['info'] = $info;
    $data['imgs'] = $img;
    $data['features'] = $features;
    $data['grpContent'] = $this->_grpContent;
    $data['frmAction'] = site_url("{$this->router->class}/update");

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('แก้ไข', site_url("{$this->router->class}/edit"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/form";

    $this->template->layout($data);
}

public function update(){
    $input = $this->input->post(null, true);
    $id = decode_id($input['id']);
    $value = $this->_build_data($input);
    $value_file = $this->_build_datas($input);
    $value_feature = $this->_build_datas_feature_update($input);
    $value_feature_insert = NULL;
    if($input['feature_name'] == ''){
        $value_feature_insert = $this->_build_datas_feature($input);
    }

    $result = $this->product_m->update($id, $value);
    if ( $result ) {
        //update
        if(!is_null($value_file)){
           $this->product_m->insert_batch_img($id,$value_file);
       }
       if(!is_null($value_feature)){
        $this->product_m->update_batch_feature($id,$value_feature);
    }
    if(!is_null($value_feature_insert)){
        $this->product_m->insert_batch_feature($id,$value_feature_insert);
    }

    Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
} else {
 Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
}
redirect(site_url("{$this->router->class}"));
}

private function _build_data($input){

    $value['title'] = $input['title'];
    $value['code'] = $input['code'];
    $value['slug'] = $input['slug'];
    $value['category_id'] = $input['category_id'];
    $value['excerpt'] = $input['excerpt'];
    $value['price'] = $input['price'];
    $value['sell'] = $input['sell'];
    $value['sku'] = $input['sku'];
    $value['detail'] = html_escape($this->input->post('detail', FALSE));
    if ( $input['mode'] == 'create' ) {
        $value['active'] = 1;
        $value['created_at'] = db_datetime_now();
        $value['created_by'] = $this->session->users['user_id'];
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = $this->session->users['user_id'];
    } else {
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = $this->session->users['user_id'];
    }

    return $value;
}

private function _build_datas($input){

    $path                = 'product';
    $upload = $this->uploadfile_library->do_uploadMultiple('file',TRUE,$path);
    $uploads = $upload['filenames'];

    foreach ($uploads as $upload ) {
        if(isset($upload['index'])){
            $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
            $outfile = $input['outfile'];
            if(!empty($outfile)){
                $this->load->helper("file");
                unlink($outfile);
            }
            $value['files'][] = $file;
        }
    }

    return $value;
}

public function _build_datas_feature($input){

    for ($i=0; $i < sizeof($input['feature_name']); $i++) { 
        $value['feature_name'][]  = $input['feature_name'][$i];
        $value['feature_dis1'][]   =  $input['feature_dis1'][$i];
        $value['feature_price1'][] =  $input['feature_price1'][$i];
        $value['feature_dis2'][]   =  $input['feature_dis2'][$i];
        $value['feature_price2'][] =  $input['feature_price2'][$i];
        $value['feature_dis3'][]   =  $input['feature_dis3'][$i];
        $value['feature_price3'][] =  $input['feature_price3'][$i];
    } 

    return $value;
}

public function _build_datas_feature_update($input){
    $input['product_id'] = decode_id($input['id']);
    $product_feature    = $this->product_m->get_rows_feature($input);
    $features = $product_feature->result();
    $i= 0;
    //echo $this->db->last_query();
    foreach ($features as $feature) {
        if(!empty($feature->feature_id) && $input['feature_id'][$i] == $feature->feature_id){
            $value['feature_id'][]    = $input['feature_id'][$i];
            $value['feature_name'][]  = $input['feature_name_edit'][$i];
            $value['feature_dis1'][]   =  $input['feature_dis_edit1'][$i];
            $value['feature_price1'][] =  $input['feature_price_edit1'][$i];
            $value['feature_dis2'][]   =  $input['feature_dis_edit2'][$i];
            $value['feature_price2'][] =  $input['feature_price_edit2'][$i];
            $value['feature_dis3'][]   =  $input['feature_dis_edit3'][$i];
            $value['feature_price3'][] =  $input['feature_price_edit3'][$i];
        }else{
            //delete product_feature
            $this->product_m->delete_feature($feature->feature_id);
        }
        $i++;
    }
    return $value;
}


public function excel(){
    set_time_limit(0);
    ini_set('memory_limit', '128M');

    $input['recycle'] = 0;
    $input['grpContent'] = $this->_grpContent;
    if ( $this->session->condition[$this->_grpContent] ) {
        $input = $this->session->condition[$this->_grpContent];
        unset($input['length']);
    }
    $info = $this->product_m->get_rows($input);
    $fileName = "product";
    $sheetName = "Sheet name";
    $sheetTitle = "Sheet title";

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();

    $styleH = excel_header();
    $styleB = excel_body();
    $colums = array(
        'A' => array('data'=>'รายการ', 'width'=>50),
        'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
        'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
        'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
    );
    $fields = array(
        'A' => 'title',
        'B' => 'excerpt',
        'C' => 'created_at',            
        'D' => 'updated_at',
    );
    $sheet->setTitle($sheetName);

        //title
    $sheet->setCellValue('A1', $sheetTitle);
    $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
    $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
    $sheet->getRowDimension(1)->setRowHeight(20);

        //header
    $rowCount = 2;
    foreach ( $colums as $colum => $data ) {
        $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
        $sheet->SetCellValue($colum.$rowCount, $data['data']);
        $sheet->getColumnDimension($colum)->setWidth($data['width']);
    }

        // data
    $rowCount++;
    foreach ( $info->result() as $row ){
        foreach ( $fields as $key => $field ){
            $value = $row->{$field};
            if ( $field == 'created_at' || $field == 'updated_at' )
                $value = datetime_table ($value);
            $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
            $sheet->SetCellValue($key . $rowCount, $value); 
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);
        $rowCount++;
    }
    $sheet->getRowDimension($rowCount)->setRowHeight(20);  
    $writer = new Xlsx($spreadsheet);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
    $writer->save('php://output');
    exit; 
}

public function pdf(){
    set_time_limit(0);
    ini_set('memory_limit', '128M');

    $input['recycle'] = 0;
    $input['grpContent'] = $this->_grpContent;
    if ( $this->session->condition[$this->_grpContent] ) {
        $input = $this->session->condition[$this->_grpContent];
        unset($input['length']);
    }
    $info = $this->product_m->get_rows($input);
    $data['info'] = $info;

    $mpdfConfig = [
        'default_font_size' => 9,
        'default_font' => 'garuda'
    ];
    $mpdf = new \Mpdf\Mpdf($mpdfConfig);
    $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
    $mpdf->WriteHTML($content);


    $download = FALSE;
    $saveFile = FALSE;
    $viewFile = TRUE;
    $fileName = "product.pdf";
    $pathFile = "uploads/pdf/product/";
    if ( $saveFile ) {
        create_dir($pathFile);
        $fileName = $pathFile.$fileName;
        $mpdf->Output($fileName, 'F');
    }
    if ( $download ) 
        $mpdf->Output($fileName, 'D');
    if ( $viewFile ) 
        $mpdf->Output();
}

public function trash(){
    $this->load->module('template');

        // toobar
    $action[1][] = action_list_view(site_url("{$this->router->class}"));
    $action[2][] = action_delete_multi(base_url("{$this->router->class}/action/delete"));
    $data['boxAction'] = Modules::run('utils/build_toolbar', $action);

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array("ถังขยะ", site_url("{$this->router->class}/trash"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/trash";

    $this->template->layout($data);
}

public function data_trash(){
    $input = $this->input->post();
    $input['recycle'] = 1;
    $info = $this->product_m->get_rows($input);
    $infoCount = $this->product_m->get_count($input);
    $column = array();
    foreach ($info->result() as $key => $rs) {
        $id = encode_id($rs->product_id);
        $action = array();
        $action[1][] = table_restore("{$this->router->class}/action/restore");         
        $active = $rs->active ? "checked" : null;
        $column[$key]['DT_RowId'] = $id;
        $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
        $column[$key]['title'] = $rs->title;
        $column[$key]['excerpt'] = $rs->excerpt;
        $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
        $column[$key]['action'] = Modules::run('utils/build_toolbar', $action);
    }
    $data['data'] = $column;
    $data['recordsTotal'] = $info->num_rows();
    $data['recordsFiltered'] = $infoCount;
    $data['draw'] = $input['draw'];
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));
}    

public function action($type=""){
    if ( !$this->_permission ) {
        $toastr['type'] = 'error';
        $toastr['lineOne'] = config_item('appName');
        $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success'] = false;
        $data['toastr'] = $toastr;
    } else {
        $input = $this->input->post();
        foreach ( $input['id'] as &$rs ) 
            $rs = decode_id($rs);
        $dateTime = db_datetime_now();
        $value['updated_at'] = $dateTime;
        $value['updated_by'] = $this->session->users['user_id'];
        $result = false;
        if ( $type == "active" ) {
            $value['active'] = $input['status'] == "true" ? 1 : 0;
            $result = $this->product_m->update_in($input['id'], $value);
        }
        if ( $type == "trash" ) {
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['user_id'];
            $result = $this->product_m->update_in($input['id'], $value);
        }
        if ( $type == "restore" ) {
            $value['active'] = 0;
            $value['recycle'] = 0;
            $result = $this->product_m->update_in($input['id'], $value);
        }
        if ( $type == "delete" ) {
            $value['active'] = 0;
            $value['recycle'] = 2;
            $result = $this->product_m->update_in($input['id'], $value);
        }   
        if ( $result ) {
            $toastr['type'] = 'success';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
        } else {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }
        $data['success'] = $result;
        $data['toastr'] = $toastr;
    }
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));        
}  

public function sumernote_img_upload(){
		//sumernote/img-upload
  $path = 'content';
  $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);

  if(isset($upload['index'])){
     $picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
 }

 echo $picture;

}

public function deletefile(){

  $arrayName = array('file' => $this->input->post('id'));
  $this->product_m->delete_img($this->input->post('id'));

  echo json_encode($arrayName);
}

}
