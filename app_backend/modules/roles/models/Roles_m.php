<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Roles_m extends MY_Model {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    public function get_rows($param) {
        $this->_condition($param);

        if ($param['order'][0]['column'] == 1)
            $columnOrder = "a.name";
        if ($param['order'][0]['column'] == 2)
            $columnOrder = "a.remark";
        if ($param['order'][0]['column'] == 3)
            $columnOrder = "a.createDate";
        if ($param['order'][0]['column'] == 4)
            $columnOrder = "a.updateDate";

        $this->db
                ->order_by($columnOrder, $param['order'][0]['dir']);

        $query = $this->db
                ->select('a.*')
                ->from('roles a')
                ->limit($param['length'], $param['start'])
                ->get();
        return $query;
    }

    public function get_count($param) {
        $this->_condition($param);
        $query = $this->db
                ->select('a.*')
                ->from('roles a')
                ->get();
        return $query->num_rows();
    }

    private function _condition($param) {

        if ($param['search']['value']) {
            $this->db
                    ->group_start()
                    ->like('a.name', $param['search']['value'])
                    ->or_like('a.remark', $param['search']['value'])
                    ->group_end();
        }
//        if ( $this->session->users['type'] != "developer" || $this->session->users['type'] != "admin" ) {
//            $this->db
//                    ->where('visible', 1);
//        }
        $this->db->where('recycle', $param['recycle']);
    }
    
    public function get_row($id) {
        
        $query = $this->db
                        ->select('*')
                        ->from('roles a')
                        ->where('role_id', $id)
                        ->get();
        return $query->row();
    }
    
    public function insert($value) {
        //arrx($value);
        $this->db->insert('roles', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value){
        $query = $this->db
                        ->where('role_id', $id)
                        ->update('roles', $value);
        return $query;
    }
    
    public function update_in($id, $value){
        $query = $this->db
                        ->where_in('role_id', $id)
                        ->update('roles', $value);
        return $query;
    }

    public function update_permission($id, $value){
        $query = $this->db
                        ->where('role_id', $id)
                        ->delete('permission_role');  
        $query = $this->db
                        ->insert_batch('permission_role', $value);
        
        return $this->db->affected_rows();
    }
    
    public function get_dropdown() {
        
        $query = $this->db
                        ->select('role_id, name')
                        ->from('roles')
                        ->where('active', 1)
                        ->where('recycle', 0)
                        ->get();
        return $query;
    }
    
    public function get_module() {
        if ( $this->session->users['type'] != "developer" ) {
            $this->db
                    ->where('isDev', 0);
        }      
        $query = $this->db
                        ->from('module')
                        ->where('active', 1)
                        //->where('isSidebar', 1)
                        ->order_by('parentId')
                        ->order_by('order')
                        ->get();
        return $query;
    }

    public function get_permission() {
           
        $query = $this->db
                        ->from('permissions')
                        ->order_by('permission_id')
                        ->get();
        return $query;
    }

    public function get_permission_role($id) {
        
        $query = $this->db
                        ->select('*')
                        ->from('permission_role a')
                        ->where('role_id', $id)
                        //->group_by('a.moduleId')
                        ->get();
        return $query;
    }

}
