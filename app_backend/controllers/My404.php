<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My404 extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		// Your own constructor code
	}

	public function index()
	{
		$this->output->set_status_header('404'); 
        $this->load->view('errors/html/404');//loading in custom error view
	}
}
