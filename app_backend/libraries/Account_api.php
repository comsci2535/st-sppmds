<?php
/**
 * Description of Login_api
 *
 * @author Don
 */
class Account_api {
    
    protected $rest = NULL;
    protected $CI;
    
    public function __construct() {
        $this->CI =& get_instance();
        $this->rest = new stdClass();
    }
    
    public function auth($username, $password) {
        $this->rest->db = $this->CI->load->database($this->CI->config->item('rest_database_group'), TRUE);
        $password = md5($password.$this->CI->config->item('encryption_key'));
        $query = $this->rest->db
                ->where('active', 1)
                ->where('recycle', 0)
                ->where('verify', 1)
                ->where('email', $username)
//                ->where('password', $password)
                ->get('user');

        if ( $query->num_rows() == 1 ) {
            $user = $query->row(1);
            $this->rest->db
                    ->where('email', $username)
                    ->where('password', $password)
                    ->update('user',array('lastLogin'=>date("Y-m-d H:i:s")));
            $value = array();
            $value['sessionId'] = session_id();
            $value['class'] = $this->CI->router->class;
            $value['function'] = $this->CI->router->method;
            $value['method'] = $this->CI->input->method();
            $value['requestUri'] = $this->CI->input->server('REQUEST_URI');
            $value['userId'] = $user->userId;
            $value['isBackend'] = 0;
            $value['timestamp'] = time();
            $value['ip'] = $this->CI->input->server('REMOTE_ADDR');
            $value['agent'] = $this->CI->agent->agent_string();
            $value['referer'] = $this->CI->agent->referrer();
            $value['isRobot'] = $this->CI->agent->is_robot();
            $value['robot'] = $this->CI->agent->robot();
            $value['isBrowser'] = $this->CI->agent->is_browser();
            $value['browser'] = $this->CI->agent->browser();
            $value['isMobile'] = $this->CI->agent->is_mobile();
            $value['mobile'] = $this->CI->agent->mobile();
            $value['platform'] = $this->CI->agent->platform();
            $this->rest->db
                    ->insert('tracking_api', $value);
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
}
