<?php 

/**
 * Library users
 */
class Nestable_library 
{
	public $CI;

	public $class;
	public $method;


	public function __construct()
	{
		$this->CI =& get_instance();

		$this->class    = $this->CI->router->fetch_class();
		$this->method   = $this->CI->router->fetch_method();

    }

   public function get_layout_ol_master($category) {
        $html_ol = '';

        $html_ol .= '<div class="dd">';
        
        if($category){
            $i=0;
          foreach ($category as $index => $category){
              if($i==0): 
                    $html_ol .= '<ol class="dd-list">';
                endif; 

                $html_ol .='<li class="dd-item" data-id="'.$category->id.'">
                <div class="dd-handle">'.$category->name.'</div>';
                
                if(sizeof($category->sub) >= 1)
                  $html_ol .=  $this->get_layout_ol_sub($category->sub);
                
                $html_ol .= '</li>';     
              
                $i++;
          }
          $html_ol .= '</ol>';    
        }

        $html_ol .= '</div>';          

        return $html_ol;

    }   


    public function get_layout_ol_sub($category) {  
        $html_ol = '';
    
        if($category){
            $i=0;
          foreach ($category as $index => $category){
                if($i==0): 
                    $html_ol .= '<ol class="dd-list">';
                endif; 

                $html_ol .='<li class="dd-item " data-id="'.$category->id.'">
                <div class="dd-handle">'.$category->name.'</div>';
                
                if(sizeof($category->sub) >= 1)
                  $html_ol .=  $this->get_layout_ol_sub($category->sub);
                
                $html_ol .= '</li>';

            $i++;   
          }

          $html_ol .= '</ol>';
        }
        return $html_ol;           
    } 


}

