<?php

/**
 *
 */
class UploadFile_library {
	public $CI;

	function __construct() {
		$this->CI = &get_instance();
	}

	public function do_upload($file,$type = TRUE ,$path) {
		$master = '';
		if (in_array(gethostname(), array('LCNB16122964'))) { 
			$master = 'C:/laragon/www/st-sppmds';
		}
		$upload_path = $master.'/uploads/'.$path.'/'.date('Y').'/'.date('m').'/';
		$upload = '/uploads/'.$path.'/'.date('Y').'/'.date('m').'/';

		if (!is_dir($upload_path ))
		{
			mkdir('./'.$upload , 0777, true);
		}
		$config['upload_path']          = 'uploads/'.$path.'/'.date('Y').'/'.date('m');
		$config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|bmp';
		//$config['max_size']             = 30000;
		$config['encrypt_name']         = TRUE;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->CI->load->library('upload', $config);

		if ( ! $this->CI->upload->do_upload($file))
		{
			$data = array('error' => $this->CI->upload->display_errors());
		}
		else
		{
			$data = array('index' => $this->CI->upload->data());
		}
		
		return $data;
		
	}

	public function do_uploadMultiple($file, $type = TRUE, $path) {
		$master = '';
		if (in_array(gethostname(), array('LCNB16122964'))) { 
			$master = 'C:/laragon/www/st-sppmds';
		}
		$upload_path=$master.'/uploads/'.$path.'/'.date('Y').'/'.date('m').'/';
		$upload = '/uploads/'.$path.'/'.date('Y').'/'.date('m').'/';

		if (!is_dir($upload_path )):
			mkdir('./'.$upload , 0777, true);
		endif;

		//config file
		$config['upload_path']          = 'uploads/'.$path.'/'.date('Y').'/'.date('m');
		$config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|bmp';
		$config['encrypt_name']         = TRUE;

		//rename input file name array
		$fileReNamePost 				= explode('[]', $file);
		$data 							= array();

		// Looping all files

		if(isset($_FILES[$fileReNamePost[0]]['name']) && count($_FILES[$fileReNamePost[0]]['name']) > 0):

			$i = 0;

		foreach($_FILES[$fileReNamePost[0]]['name'] as $filename):

			$_FILES['file[]']['name'] 		=  $filename;
			$_FILES['file[]']['type'] 		=  $_FILES[$fileReNamePost[0]]['type'][$i];
			$_FILES['file[]']['tmp_name'] 	=  $_FILES[$fileReNamePost[0]]['tmp_name'][$i];
			$_FILES['file[]']['error'] 		=  $_FILES[$fileReNamePost[0]]['error'][$i];
			$_FILES['file[]']['size'] 		=  $_FILES[$fileReNamePost[0]]['size'][$i];
			$config['file_name'] 			=  $filename;

				//Load upload library
			$this->CI->load->library('upload', $config);

			if (!$this->CI->upload->do_upload('file[]')):
				$data['filenames'][] = array('error' => $this->CI->upload->display_errors());
			else:
				$data['filenames'][] = array('index' => $this->CI->upload->data());
			endif;
			$i++;

		endforeach;	

	endif;

	return $data;
}
}

?>
