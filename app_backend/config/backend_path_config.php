<?php 

$CI =& get_instance();

$config['root_url']    = str_replace('backend/' ,'' , $CI->config->base_url());

$config['template'] = $config['root_url'] . 'template/metronic/';

$config['assets'] = $config['template']. 'assets/';

$config['scripts'] = $config['root_url'] . 'scripts/backend/';
 ?>