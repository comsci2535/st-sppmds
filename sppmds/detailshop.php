<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include("view/meta.php"); ?>
	<title>ชื่อสินค้า | SPPMDS</title>
</head>

<body class="stretched">
	<div id="wrapper" class="clearfix">

		<header id="header">
			<div id="header-wrap">
				<div class="container clearfix">
					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
					<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
					</div>
					<nav id="primary-menu">
						<ul>
							<li><a href="index.php"><div>หน้าแรก</div></a></li>
							<li><a href="about.php"><div>เกี่ยวกับเรา</div></a></li>
							<li class="current"><a href="shop.php"><div>สินค้า</div></a>
								<ul>
									<li><a href="#"><div>ของตกแต่งบ้าน</div></a></li>
									<li><a href="#"><div>แก้ว</div></a></li>
									<li><a href="#"><div>กระเป๋าผ้า</div></a></li>
									<li><a href="#"><div>ของเล่น</div></a></li>
									<li><a href="#"><div>ตุ๊กตา</div></a></li>
									<li><a href="#"><div>หมวก</div></a></li>
								</ul>
							</li>
							<li><a href="portfolio.php"><div>ผลงานที่ผ่านมา</div></a></li>
							<li><a href="contact.php"><div>ติดต่อเรา</div></a></li>
						</ul>
						<div id="top-search">
							<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
							<form action="search.html" method="get">
								<input type="text" name="q" class="form-control" value="" placeholder="คำที่ค้นหา ..">
							</form>
						</div>
					</nav>
				</div>
			</div>
		</header>

		<section id="page-title">
			<div class="container clearfix">
				<h1>สินค้า</h1>
				<span>เลือกสินค้าตามใจคุณ</span>
				<ol class="breadcrumb">
					<li><a href="#">หน้าแรก</a></li>
					<li class="active">สินค้า</li>
				</ol>
			</div>
		</section>

		<div class="clear"></div>

		<section id="content">
			<div class="content-wrap">

				<div class="container clearfix">
					<div class="postcontent nobottommargin clearfix col_last">
						<div class="single-product">
							<div class="product">
								<div class="col_half">
									<div class="product-image">
										<div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
											<div class="flexslider">
												<div class="slider-wrap" data-lightbox="gallery">
													<div class="slide" data-thumb="images/shop/thumbs/dress/3.jpg"><a href="images/shop/dress/3.jpg" title="Pink Printed Dress - Front View" data-lightbox="gallery-item"><img src="images/shop/dress/3.jpg" alt="Pink Printed Dress"></a></div>
													<div class="slide" data-thumb="images/shop/thumbs/dress/3-1.jpg"><a href="images/shop/dress/3-1.jpg" title="Pink Printed Dress - Side View" data-lightbox="gallery-item"><img src="images/shop/dress/3-1.jpg" alt="Pink Printed Dress"></a></div>
													<div class="slide" data-thumb="images/shop/thumbs/dress/3-2.jpg"><a href="images/shop/dress/3-2.jpg" title="Pink Printed Dress - Back View" data-lightbox="gallery-item"><img src="images/shop/dress/3-2.jpg" alt="Pink Printed Dress"></a></div>
												</div>
											</div>
										</div>
										<div class="sale-flash">Sale!</div>
									</div>
								</div>

								<div class="col_half col_last product-desc">

									<div class="col_full nobottommargin">
										<div class="tabs clearfix nobottommargin" id="tab-1">
											<ul class="tab-nav clearfix">
												<li><a href="#tabs-1"><i class="icon-align-justify2"></i><span class="hidden-xs"> ขนาดไซส์ S</span></a></li>
												<li><a href="#tabs-2"><i class="icon-info-sign"></i><span class="hidden-xs"> ขนาดไซส์ M</span></a></li>
												<li><a href="#tabs-3"><i class="icon-star3"></i><span class="hidden-xs"> ขนาดไซส์ L</span></a></li>
											</ul>
											<div class="tab-container">
												<div class="tab-content clearfix" id="tabs-1">
													<table class="table table-striped table-bordered">
														<tbody>
															<tr>
																<td>1-50 ชิ้น</td>
																<td>200 บาท</td>
															</tr>
															<tr>
																<td>51-100 ชิ้น</td>
																<td>150 บาท</td>
															</tr>
															<tr>
																<td>101 ชิ้นขึ้นไป</td>
																<td>80 บาท</td>
															</tr>
														</tbody>
													</table>
												</div>
												<div class="tab-content clearfix" id="tabs-2">
													<table class="table table-striped table-bordered">
														<tbody>
															<tr>
																<td>1-50 ชิ้น</td>
																<td>200 บาท</td>
															</tr>
															<tr>
																<td>51-100 ชิ้น</td>
																<td>150 บาท</td>
															</tr>
															<tr>
																<td>101 ชิ้นขึ้นไป</td>
																<td>80 บาท</td>
															</tr>
														</tbody>
													</table>
												</div>
												<div class="tab-content clearfix" id="tabs-3">
													<table class="table table-striped table-bordered">
														<tbody>
															<tr>
																<td>1-50 ชิ้น</td>
																<td>200 บาท</td>
															</tr>
															<tr>
																<td>51-100 ชิ้น</td>
																<td>150 บาท</td>
															</tr>
															<tr>
																<td>101 ชิ้นขึ้นไป</td>
																<td>80 บาท</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>

									<div class="clear"></div>
									<div class="line"></div>


										<a href="#" class="button button-rounded button-reveal button-large button-green button-light tright"><i class="icon-line-arrow-right"></i><span style="color: #fff;">สั่งซื้อสินค้า</span></a>
										<a href="#" class="button button-rounded button-reveal button-large button-red button-light tright"><i class="icon-line-arrow-right"></i><span style="color: #fff;">ขอใบเสนอราคา</span></a>

									<div class="clear"></div>
									<div class="line"></div>

									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero velit id eaque ex quae laboriosam nulla optio doloribus! Perspiciatis, libero, neque, perferendis at nisi optio dolor!</p>
									<p>Perspiciatis ad eveniet ea quasi debitis quos laborum eum reprehenderit eaque explicabo assumenda rem modi.</p>
									<ul class="iconlist">
										<li><i class="icon-caret-right"></i> Dynamic Color Options</li>
										<li><i class="icon-caret-right"></i> Lots of Size Options</li>
										<li><i class="icon-caret-right"></i> 30-Day Return Policy</li>
									</ul>

									<div class="panel panel-default product-meta">
										<div class="panel-body">
											<span itemprop="productID" class="sku_wrapper">SKU: <span class="sku">8465415</span></span>
											<span class="posted_in">Category: <a href="#" rel="tag">Dress</a>.</span>
											<span class="tagged_as">Tags: <a href="#" rel="tag">Pink</a>, <a href="#" rel="tag">Short</a>, <a href="#" rel="tag">Dress</a>, <a href="#" rel="tag">Printed</a>.</span>
										</div>
									</div>

									<div class="si-share noborder clearfix">
										<span>Share:</span>
										<div>
											<a href="#" class="social-icon si-borderless si-facebook">
												<i class="icon-facebook"></i>
												<i class="icon-facebook"></i>
											</a>
										</div>
									</div>

								</div>

								<div class="col_full nobottommargin">

									<p>Pink printed dress,  woven, round neck with a keyhole and buttoned closure at the back, sleeveless, concealed zip up at left side seam, belt loops along waist with slight gathers beneath, brand appliqu?? above left front hem, has an attached lining.</p>
									Comes with a white, slim synthetic belt that has a tang clasp.
									<br>
									<img src="images/shop/dress/3.jpg" alt="Pink Printed Dress">

									<div class="line"></div>

								</div>
							</div>
						</div>

						<div class="clear"></div>

					<!--
						<div class="col_full nobottommargin">

							<h4>สินค้าแนะนำ</h4>

							<div id="oc-product" class="owl-carousel product-carousel carousel-widget" data-margin="30" data-pagi="false" data-autoplay="5000" data-items-xxs="1" data-items-sm="2" data-items-lg="4">

								<div class="oc-item">
									<div class="product iproduct clearfix">
										<div class="product-image">
											<a href="#"><img src="images/shop/dress/1.jpg" alt="Checked Short Dress"></a>
											<div class="sale-flash">50% Off*</div>
											<div class="product-overlay">
												<a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
												<a href="images/shop/dress/1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
											</div>
										</div>
										<div class="product-desc center">
											<div class="product-title"><h3><a href="#">Checked Short Dress</a></h3></div>
											<div class="product-price"><del>฿ 24.99</del> <ins>฿ 12.49</ins></div>
										</div>
									</div>
								</div>

								<div class="oc-item">
									<div class="product iproduct clearfix">
										<div class="product-image">
											<a href="#"><img src="images/shop/pants/1-1.jpg" alt="Slim Fit Chinos"></a>
											<div class="product-overlay">
												<a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
												<a href="images/shop/pants/1-1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
											</div>
										</div>
										<div class="product-desc center">
											<div class="product-title"><h3><a href="#">Slim Fit Chinos</a></h3></div>
											<div class="product-price">฿ 39.99</div>
										</div>
									</div>
								</div>

								<div class="oc-item">
									<div class="product iproduct clearfix">
										<div class="product-image">
											<a href="#"><img src="images/shop/shoes/1-1.jpg" alt="Dark Brown Boots"></a>
											<div class="product-overlay">
												<a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
												<a href="images/shop/shoes/1-1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
											</div>
										</div>
										<div class="product-desc center">
											<div class="product-title"><h3><a href="#">Dark Brown Boots</a></h3></div>
											<div class="product-price">฿ 49</div>
										</div>
									</div>
								</div>

								<div class="oc-item">
									<div class="product iproduct clearfix">
										<div class="product-image">
											<a href="#"><img src="images/shop/dress/2.jpg" alt="Light Blue Denim Dress"></a>
											<div class="product-overlay">
												<a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
												<a href="images/shop/dress/2.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
											</div>
										</div>
										<div class="product-desc center">
											<div class="product-title"><h3><a href="#">Light Blue Denim Dress</a></h3></div>
											<div class="product-price">฿ 19.95</div>
										</div>
									</div>
								</div>

								<div class="oc-item">
									<div class="product iproduct clearfix">
										<div class="product-image">
											<a href="#"><img src="images/shop/sunglasses/1.jpg" alt="Unisex Sunglasses"></a>
											<div class="sale-flash">Sale!</div>
											<div class="product-overlay">
												<a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> ดูรายละเอียดสินค้า</span></a>
												<a href="images/shop/sunglasses/1.jpg" class="item-quick-view" data-lightbox="image"><i class="icon-zoom-in2"></i><span> ดูภาพสินค้า</span></a>
											</div>
										</div>
										<div class="product-desc center">
											<div class="product-title"><h3><a href="#">Unisex Sunglasses</a></h3></div>
											<div class="product-price"><del>฿ 19.99</del> <ins>฿ 11.99</ins></div>
										</div>
									</div>
								</div>

							</div>

						</div>
					-->

					</div>

					<div class="sidebar nobottommargin">
						<div class="sidebar-widgets-wrap">

							<div class="widget widget_links clearfix">
								<h4>หมวดสินค้า</h4>
								<ul>
									<li><a href="#"><div>ของตกแต่งบ้าน</div></a></li>
									<li><a href="#"><div>แก้ว</div></a></li>
									<li><a href="#"><div>กระเป๋าผ้า</div></a></li>
									<li><a href="#"><div>ของเล่น</div></a></li>
									<li><a href="#"><div>ตุ๊กตา</div></a></li>
									<li><a href="#"><div>หมวก</div></a></li>
								</ul>
							</div>

							<div class="widget clearfix">
								<h4>สินค้าแนะนำ</h4>
								<div id="post-list-footer">
									<div class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="images/shop/small/1.jpg" alt="Image"></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Blue Round-Neck Tshirt</a></h4>
											</div>
											<ul class="entry-meta">
												<li class="color">฿ 29.99</li>
											</ul>
										</div>
									</div>
									<div class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="images/shop/small/6.jpg" alt="Image"></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Checked Short Dress</a></h4>
											</div>
											<ul class="entry-meta">
												<li class="color">฿ 23.99</li>
											</ul>
										</div>
									</div>
									<div class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="images/shop/small/7.jpg" alt="Image"></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Light Blue Denim Dress</a></h4>
											</div>
											<ul class="entry-meta">
												<li class="color">฿ 19.99</li>
											</ul>
										</div>
									</div>
								</div>
							</div>

							<div class="widget clearfix">
								<h4>สินค้าในหมวดเดียวกัน</h4>
								<div class="widget-last-view">
									<div class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="images/shop/small/3.jpg" alt="Image"></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Round-Neck Tshirt</a></h4>
											</div>
											<ul class="entry-meta">
												<li class="color">฿ 15</li>
											</ul>
										</div>
									</div>
									<div class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="images/shop/small/10.jpg" alt="Image"></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Green Trousers</a></h4>
											</div>
											<ul class="entry-meta">
												<li class="color">฿ 19</li>
											</ul>
										</div>
									</div>
									<div class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="images/shop/small/11.jpg" alt="Image"></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Silver Chrome Watch</a></h4>
											</div>
											<ul class="entry-meta">
												<li class="color">฿ 34.99</li>
											</ul>
										</div>
									</div>
								</div>
							</div>

							<div class="widget clearfix">
								<div id="fb-root"></div>
								<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v3.2&appId=289137874657&autoLogAppEvents=1"></script>
								<div class="fb-page" data-href="https://www.facebook.com/SakidloCode/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/SakidloCode/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/SakidloCode/">SakidloCode</a></blockquote></div>
							</div>

							<div class="widget clearfix">
								<div id="oc-clients-full" class="owl-carousel image-carousel carousel-widget" data-items="1" data-margin="10" data-loop="true" data-nav="false" data-autoplay="5000" data-pagi="false">
									<div class="oc-item"><a href="#"><img src="images/clients/1.png" alt="Clients"></a></div>
									<div class="oc-item"><a href="#"><img src="images/clients/2.png" alt="Clients"></a></div>
									<div class="oc-item"><a href="#"><img src="images/clients/3.png" alt="Clients"></a></div>
									<div class="oc-item"><a href="#"><img src="images/clients/4.png" alt="Clients"></a></div>
									<div class="oc-item"><a href="#"><img src="images/clients/5.png" alt="Clients"></a></div>
									<div class="oc-item"><a href="#"><img src="images/clients/6.png" alt="Clients"></a></div>
									<div class="oc-item"><a href="#"><img src="images/clients/7.png" alt="Clients"></a></div>
									<div class="oc-item"><a href="#"><img src="images/clients/8.png" alt="Clients"></a></div>
								</div>
							</div>
						</div>

					</div><!-- .sidebar end -->

				</div>
			</div>
		</section>

		<?php include("view/footer.php"); ?>

	</div>

	<?php include("view/script.php"); ?>

</body>
</html>
