<?php
/*
ini_set('display_errors', 1);
error_reporting(E_ALL);
ini_set("display_errors", 1);
*/

header("Cache-Control: no-cache, must-revalidate");
header('Content-Type: text/html; charset=utf-8');
require_once('stmadmin/fontdriver.php');

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>รถมอเตอร์ไซค์ ฮอนด้า รถจักรยานยนต์ ฮอนด้า ฮอนด้าอ้อมน้อย อ้อมน้อยกรุ๊ป โอเอ็นซิตี้กรุ๊ป omnoigroup</title>
		<meta name="description" content="รถมอเตอร์ไซค์ ฮอนด้า รถจักรยานยนต์ ฮอนด้า ฮอนด้าอ้อมน้อย อ้อมน้อยกรุ๊ป โอเอ็นซิตี้กรุ๊ป omnoigroup">
		<meta name="keywords" content="รถมอเตอร์ไซค์, ฮอนด้า, รถจักรยานยนต์, ฮอนด้า, ฮอนด้าอ้อมน้อย, อ้อมน้อยกรุ๊ป, โอเอ็นซิตี้กรุ๊ป, omnoigroup, honda">
		<link href="css/custom.css" rel="stylesheet">
		<?php include("view/meta.php"); ?>
	</head>
	<body>

		<?php include("view/head.php"); ?>
								<ul class="collapse navbar-collapse navbar-nav-menu" id="nav">
									<li class="active"><a class="no-decoration" href="index.php">หน้าหลัก</a><span class="main-nav__separator"><span></span><span></span><span></span></span></li>
									<li><a class="no-decoration" href="product.php">รถมอเตอร์ไซค์</a><span class="main-nav__separator"><span></span><span></span><span></span></span></li>
									<li><a class="no-decoration" href="motorcycle_second_hand.php">มอเตอร์ไซค์มือสอง</a><span class="main-nav__separator"><span></span><span></span><span></span></span></li>
									<li><a class="no-decoration" href="promotion.php">โปรโมชั่น</a><span class="main-nav__separator"><span></span><span></span><span></span></span></li>
									<li><a class="no-decoration" href="activity.php">กิจกรรม</a><span class="main-nav__separator"><span></span><span></span><span></span></span></li>
									<li><a class="no-decoration" href="racing.php">Racing</a><span class="main-nav__separator"><span></span><span></span><span></span></span></li>
									<li><a class="no-decoration" href="branch.php">สาขา</a><span class="main-nav__separator"><span></span><span></span><span></span></span></li>
									<li><a class="no-decoration" href="contactus.php">ติดต่อเรา</a></li>
								</ul>
								<?php include("view/search.php"); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</nav><!--top-nav-->

		<?php include("view/marquee.php"); ?>

		<?php include("view/slide.php"); ?>

		<section class="listings">
			<div class="container">
				<header class="tab-header clearfix wow slideInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
					<h2 class="title title--main pull-left"><span class="title__bold">รถมอเตอร์ไซค์ รถจักรยานยนต์</span> <br>มาใหม่<span class="line line--title"><span class="line__first"></span><span class="line__second"></span></span></h2>
				</header>

				<div class="row isotope">

					<?php

					$query="select Count(news_id) AS CountRows from news";
					//echo $query;

					$result= mysqli_query($mysqli, $query) or die('Error #1');
					$Nump=mysqli_num_rows($result,0,"CountRows");

					$ShowAll = 9; // จำนวน thumbnail

					$ndiv = $Nump % $ShowAll;
					if($ndiv==0)
					$npage = $Nump / $ShowAll;
					else
					$npage = (int)($Nump / $ShowAll)+1;

					if($page1==0)
					{
					$page1=1;
					}

					$start = ($page1 * $ShowAll) - $ShowAll;


					$qry_new = mysqli_query($mysqli, "SELECT * FROM `news` WHERE `sta_mn_id` = 'h' ORDER By `show_id` ASC LIMIT $start, $ShowAll");
					$count=mysqli_num_rows($qry_new);
					$rows_new=true;
					?>

					<?php
					if($count==0){
						echo '<br /><br /><h2 class="noProduct">ไม่มีข้อมูลรถใหม่</h2><br /><br />';
					}
						else
					{
					?>

					<?php

					$intRows=0;

					$productsIndex =0;

					while($rows_new){

							for($i=0;$i<3;$i++){
					$intRows++;

						if($rows_new=mysqli_fetch_assoc($qry_new)){

					$sql_img="SELECT * FROM news_img  WHERE news_id='".$rows_new['news_id']."' ORDER BY news_img_id ASC ";
					$qry_img=mysqli_query($mysqli, $sql_img);
					$rows_img=mysqli_fetch_array($qry_img, MYSQLI_ASSOC);

					?>

					<div class="col-md-4 col-xs-8 isotope-item wow zoomInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
						<a href="detailbike.php?news_id=<?=$rows_new['news_id'];?>" class="listing-anons equal-height-item listing-anons--home triangle triangle--big line-down no-decoration">
							<div class="listing-anons__img">
								<img src="stmadmin/img_large/<?=$rows_img['news_img_large'];?>" class="img-responsive" alt="<?=$rows_new['head_news'];?>" />
							</div>
							<div class="listing-anons__title">
								<h4 class="name"><?=$rows_new['head_news'];?></h4>
							</div>
							<div class="listing-anons__hidden">
								<h3><?=$rows_new['head_news'];?></h3>
							</div>
						</a>
					</div>
					<?php

					$productsIndex++;
								if ($productsIndex %3==0){
									echo '<div style="float:left; clear:both;"></div>';
								}

					?>
					<?php

					if(($intRows)%3==0)
								;}
							;}
						;}

					?>
					<?php ;} ?>

				</div>

			</div>
		</section>

		<?php include("view/service.php"); ?>

		<?php include("view/act.php"); ?>

		<?php include("view/partner.php"); ?>

		<?php include("view/branch.php"); ?>

		<?php include("view/footer.php"); ?>

	</body>
</html>
